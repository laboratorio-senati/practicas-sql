# PRACTICAS SQL

codigos sql:

```sql

DESCRIBE DEPARTMENTS;

DESCRIBE EMPLOYEES;

describe jobs;

select * from employees;

select * from countries;

select * from departments;

select department_id, manager_id, location_id from departments;

select hire_date from employees;

select first_name, last_name from employees;

select employee_id, phone_number, email, salary from employees;

select first_name, last_name, salary * 1.15 + 1000 salario_aumentado from employees;

select 5000 * 1500 + null + 325 resultado from employees;

select first_name || last_name from employees;

select first_name || ' ' || last_name from employees;

select concat(concat(first_name, ' '), last_name) from employees;

select EMPLOYEE_ID,LAST_NAME, DEPARTMENT_ID FROM EMPLOYEES;

SELECT DEPARTMENT_ID, DEPARTMENT_NAME, LOCATION_ID FROM departments;

SELECT EMPLOYEE_ID, LAST_NAME, JOB_ID FROM EMPLOYEES;

SELECT JOB_ID, JOB_TITLE FROM JOBS;

SELECT FIRST_NAME, LAST_NAME, JOB_ID, JOB_TITLE FROM employees NATURAL JOIN JOBS WHERE DEPARTMENT_ID > 80 ;


select * from departments;

select * from locations;

select department_name, city from departments natural join locations;

select department_name from departments;

select last_name from employees;

select last_name, department_name from employees cross join departments;


select employee_id, department_id, department_name from employees natural join departments;

select last_name, department_name from employees natural join departments;

select department_id, department_name, location_id, city from departments natural join locations;

select department_id, department_name, location_id, city from departments NATURAL JOIN locations where department_id >=20 and department_id <=50; 




select first_name, last_name, department_id, department_name from employees join departments using (department_id) ;

select * from employees;

select * from departments;

select first_name, last_name, department_id, department_name from employees join departments using (department_id) where department_name = 'Accounting' ;

select * from employees; 

select * from jobs;

select last_name, job_title from employees e JOIN jobs j on (e.job_id = j.job_id);

select last_name, job_title from employees e JOIN jobs j on (e.job_id = j.job_id) where last_name LIKE 'H%';

select last_name as "nombre", department_name as "departamento", city as "ciudad" from employees join departments using (department_id) join locations using (location_id); 

select location_id from locations where location_id =1400;

select city, department_name, location_id, department_id from departments natural join locations where department_id in(10,20,30);

select job_id, last_name, first_name, email, job_title from employees natural join jobs where job_title = 'programmer';

```