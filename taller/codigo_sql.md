

CREATE TABLE cliente (
    nombre           VARCHAR2(20) NOT NULL,
    apellidos        VARCHAR2(20) NOT NULL,
    dni              NUMBER(9) NOT NULL,
    direccion        VARCHAR2(40) NOT NULL,
    fecha_nacimiento NUMBER(15) NOT NULL
);

ALTER TABLE cliente ADD CONSTRAINT cliente_pk PRIMARY KEY ( dni );

CREATE TABLE cliente_producto (
    cliente_dni     NUMBER(9) NOT NULL,
    producto_codigo VARCHAR2(25) NOT NULL
);

ALTER TABLE cliente_producto ADD CONSTRAINT cliente_producto_pk PRIMARY KEY ( cliente_dni,
                                                                              producto_codigo );

CREATE TABLE producto (
    nombre_producto VARCHAR2(50) NOT NULL,
    codigo_sku      VARCHAR2(25) NOT NULL,
    precio_unitario FLOAT(7)
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( codigo_sku );

CREATE TABLE proveedor (
    ruc              VARCHAR2(11) NOT NULL,
    razon_social     VARCHAR2(50) NOT NULL,
    domicilio_fiscal VARCHAR2(150) NOT NULL,
    producto_codigo  VARCHAR2(25)
);

ALTER TABLE proveedor ADD CONSTRAINT proveedor_pk PRIMARY KEY ( ruc );

ALTER TABLE cliente_producto
    ADD CONSTRAINT cliente_producto_cliente_fk FOREIGN KEY ( cliente_dni )
        REFERENCES cliente ( dni );

ALTER TABLE cliente_producto
    ADD CONSTRAINT cliente_producto_producto_fk FOREIGN KEY ( producto_codigo )
        REFERENCES producto ( codigo_sku );

ALTER TABLE proveedor
    ADD CONSTRAINT proveedor_producto_fk FOREIGN KEY ( producto_codigo )
        REFERENCES producto ( codigo_sku );

