# Solucionario - Secuencias (create sequence - currval - nextval - drop sequence)

## Ejercicios propuestos

# Ejercicio 01

## 1. Elimine la tabla "empleados":

```sql
drop table empleados;
```

## 2. Cree la tabla:

```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```

## 3. Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (999), valor inicial (100), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.

```sql
DROP SEQUENCE IF EXISTS sec_legajoempleados;

CREATE SEQUENCE sec_legajoempleados
    MINVALUE 1
    MAXVALUE 999
    START WITH 100
    INCREMENT BY 2
    NO CYCLE;

```
## 4. Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria:

```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

insert into empleados
values (sec_legajoempleados.nextval,'26777888','Estela Esper');
```

## 5. Recupere los registros de "libros" para ver los valores de clave primaria.

```sql
SELECT legajo FROM empleados;
```
## 6. Vea el valor actual de la secuencia empleando la tabla "dual". Retorna 108.

```sql
select * from empleados;
```
## 7. Recupere el valor siguiente de la secuencia empleando la tabla "dual" Retorna 110.

```sql
SELECT empleados.NEXTVAL FROM dual;

```
## 8. Ingrese un nuevo empleado (recuerde que la secuencia ya tiene el próximo valor, emplee "currval" para almacenar el valor de legajo)

```sql
insert into empleados
values (sec_legajoempleados.currval,'26777537','jose luis');
```
## 9. Recupere los registros de "libros" para ver el valor de clave primaria ingresado anteriormente.

```sql
select *from all_sequences where sequence_name='SEC_CODIGOLIBROS';
```
## 10. Incremente el valor de la secuencia empleando la tabla "dual" (retorna 112)

```sql
UPDATE legado SET NEXTVAL = NEXTVAL + 2;
```
## 11. Ingrese un empleado con valor de legajo "112".

```sql
insert into empleados
values (sec_legajoempleados.currval,'26777537','jose ');

```
## 12. Intente ingresar un registro empleando "currval":

```sql
insert into empleados
values (sec_legajoempleados.currval,'29000111','Hector Huerta');
```

Mensaje de error porque el legajo está repetido y la clave primaria no puede repetirse.

## 13. Incremente el valor de la secuencia. Retorna 114.

```sql
UPDATE empleados_seq SET NEXTVAL = NEXTVAL + 2;

```
## 14. Ingrese el registro del punto 11.

```sql
insert into empleados
values (sec_legajoempleados.currval,'26777537','lia');
```
Ahora si lo permite, pues el valor retornado por "currval" no está repetido en la tabla "empleados".

## 15. Recupere los registros.

```sql
select * from empleados;
```
## 16. Vea las secuencias existentes y analice la información retornada.

```sql
select legado, documento, nombre from empleados;
```
Debe aparecer "sec_legajoempleados".

