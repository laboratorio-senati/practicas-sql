# Solucionario - Valores por defecto (default)

## Ejercicios propuestros

# Ejercicio 01

## 1.Elimine la tabla "visitantes"
codigo sql:

```sql
DROP TABLE VISITANTES;
```
## 2.Cree la tabla con la siguiente estructura:
codigo sql:

```sql
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1) default 'f',
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Cordoba',
    telefono varchar(11),
    mail varchar(30) default 'no tiene',
    montocompra number (6,2)
);

```
## 3.Analice la información que retorna la siguiente consulta:
codigo sql:

```sql
select column_name,nullable,data_default
from user_tab_columns where TABLE_NAME = 'VISITANTES';

```
## 4.Ingrese algunos registros sin especificar valores para algunos campos para ver cómo opera la cláusula "default":
codigo sql:

```sql
insert into visitantes (domicilio,ciudad,telefono,mail,montocompra)
values ('Colon 123','Cordoba','4334455','<juanlopez@hotmail.com>',59.80);
insert into visitantes (nombre,edad,sexo,telefono,mail,montocompra)
values ('Marcos Torres',29,'m','4112233','<marcostorres@hotmail.com>',60);
insert into visitantes (nombre,edad,sexo,domicilio,ciudad)
values ('Susana Molina',43,'f','Bulnes 345','Carlos Paz');

```
## 5.Recupere todos los registros. Los campos de aquellos registros para los cuales no se ingresó valor almacenaron el valor por defecto ("null" o el especificado con "default").
codigo sql:

```sql
SELECT * FROM VISITANTES;
```
## 6.Use la palabra "default" para ingresar valores en un "insert"
codigo sql:

```sql
insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono,mail,montocompra) values ('juan lucas','9','m','jiron.lima','ecuador','736454','seue@gmail.com',default);
```
## 7.Recupere el registro anteriormente ingresado.
codigo sql:

```sql
select * from visitantes;
```
# Ejercicio 02

## 1.Elimine la tabla "prestamos"
codigo sql:

```sql
drop table prestamos;
```
## 2.Cree la tabla:
codigo sql:

```sql
create table prestamos(
    titulo varchar2(40) not null,
    documento char(8) not null,
    fechaprestamo date not null,
    fechadevolucion date,
    devuelto char(1) default 'n'
);

```
## 3.Consulte "user_tab_columns" y analice la información. Hay 3 campos que no admiten valores nulos y 1 solo campo con valor por defecto.
codigo sql:

```sql
SELECT titulo, documento, fechaprestamo FROM prestamos WHERE titulo = 'prestamos';

```
## 4.Ingrese algunos registros omitiendo el valor para los campos que lo admiten.
codigo sql:

```sql
insert into prestamos (titulo,documento,fechaprestamo,fechadevolucion,devuelto) values('avenger','KGFFYD','12/05/2023');
```
## 5.Seleccione todos los registros.
codigo sql:

```sql
select * from prestamos;
```
## 6.Ingrese un registro colocando "default" en los campos que lo admiten y vea cómo se almacenó.
codigo sql:

```sql
insert into prestamos (titulo,documento,fechaprestamo,fechadevolucion,devuelto) values('avenger','KGFFYD','12/05/2023','01/06/2023',default);
```
## 7.Intente ingresar un registro colocando "default" como valor para un campo que no admita valores nulos y no tenga definido un valor por defecto.
codigo sql:

```sql
insert into prestamos (titulo,documento,fechaprestamo,fechadevolucion,devuelto) values('el laverinto',default,'12/05/2023','01/06/2023',default);
```