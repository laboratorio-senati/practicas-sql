# Solucionario - Combinar más de 2 tablas

## Ejercicios propuestos

## 1. Elimine las tablas:

```sql
drop table socios;
drop table deportes;
drop table inscriptos;
```

## 2. Cree las tablas con las siguientes estructuras:

```sql
create table socios(
    documento char(8) not null, 
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table deportes(
    codigo number(2),
    nombre varchar2(20),
    profesor varchar2(15),
    primary key(codigo)
);

create table inscriptos(
    documento char(8) not null, 
    codigodeporte number(2) not null,
    año char(4),
    matricula char(1),--'s'=paga, 'n'=impaga
    primary key(documento,codigodeporte,año)
);
```

## 3. Ingrese algunos registros en "socios":

```sql
insert into socios values('22222222','Ana Acosta','Avellaneda 111');
insert into socios values('23333333','Betina Bustos','Bulnes 222');
insert into socios values('24444444','Carlos Castro','Caseros 333');
insert into socios values('25555555','Daniel Duarte','Dinamarca 44');
```

## 4. Ingrese algunos registros en "deportes":

```sql
insert into deportes values(1,'basquet','Juan Juarez');
insert into deportes values(2,'futbol','Pedro Perez');
insert into deportes values(3,'natacion','Marina Morales');
insert into deportes values(4,'tenis','Marina Morales');
```

## 5. Inscriba a varios socios en el mismo deporte en el mismo año:

```sql
insert into inscriptos values ('22222222',3,'2016','s');
insert into inscriptos values ('23333333',3,'2016','s');
insert into inscriptos values ('24444444',3,'2016','n');
```

## 6. Inscriba a un mismo socio en el mismo deporte en distintos años:

```sql
insert into inscriptos values ('22222222',3,'2015','s');
insert into inscriptos values ('22222222',3,'2017','n');
```

## 7. Inscriba a un mismo socio en distintos deportes el mismo año:

```sql
insert into inscriptos values ('24444444',1,'2016','s');
insert into inscriptos values ('24444444',2,'2016','s');
```

## 8. Ingrese una inscripción con un código de deporte inexistente y un documento de socio que no exista en "socios":

```sql
insert into inscriptos values ('26666666',0,'2016','s');
```

## 9. Muestre el nombre del socio, el nombre del deporte en que se inscribió y el año empleando diferentes tipos de join (8 filas):

```sql
SELECT s.nombre, d.nombre, i.año
FROM socios AS s
INNER JOIN inscriptos AS i ON s.documento = i.documento
INNER JOIN deportes AS d ON i.codigo = d.codigo;

```
## 10. Muestre todos los datos de las inscripciones (excepto los códigos) incluyendo aquellas inscripciones cuyo código de deporte no existe en "deportes" y cuyo documento de socio no se encuentra en "socios" (10 filas)

```sql
SELECT *, d.nombre, s.nombre
FROM inscriptos AS i
LEFT JOIN deportes AS d ON i.codigo = d.codigo
LEFT JOIN socios AS s ON i.documento = s.documento
WHERE d.codigo IS NULL OR s.documento IS NULL;

```
## 11. Muestre todas las inscripciones del socio con documento "22222222" (3 filas)

```sql
SELECT i.*
FROM inscriptos AS i
INNER JOIN socios AS s ON i.documento = s.documento
WHERE s.documento = '22222222';

```