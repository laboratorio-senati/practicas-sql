# Solucionario - Recuperar algunos registros (where)

## Ejercicios propuestos

# Ejercicio 01

## 1.Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):
codigo sql:

```sql

create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
salida sql:

```sh

Table AGENDA creado.
```
## 2.Visualice la estructura de la tabla "agenda" (4 campos)
codigo sql:

```sql

describe agenda;
```
salida sql:

```sh

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(30) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 
```

## 3.Ingrese los siguientes registros ("insert into"):
codigo sql:

```sql

insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

## 4.Seleccione todos los registros de la tabla (5 registros)
codigo sql:

```sql


select * from agenda;
```
salida sql:

```sh

Acosta	    Ana	Colon 123	4234567
Bustamante	Betina	Avellaneda 135	4458787
Lopez	    Hector	Salta 545	4887788
Lopez	    Luis	Urquiza 333	4545454
Lopez	    Marisa	Urquiza 333	4545454
```

## 5.Seleccione el registro cuyo nombre sea "Marisa" (1 registro)
codigo sql:

```sql

select * from agenda where nombre = 'Marisa';
```
salida sql:

```sh

Lopez	Marisa	Urquiza 333	4545454
```

## 6.Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)
codigo sql:

```sql

select * from agenda where apellido = 'Lopez';
```
salida sql:

```sh

Lopez	Hector	Salta 545	4887788
Lopez	Luis	Urquiza 333	4545454
Lopez	Marisa	Urquiza 333	4545454
```
## 7.Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)
codigo sql:

```sql

select * from agenda where telefono = '4545454';
```
salida sql:

```sh

Lopez	Luis	Urquiza 333	4545454
Lopez	Marisa	Urquiza 333	4545454
```

## 8.Elimina la tabla agenda
codigo sql:

```sql

drop table agenda;
```
salida sql:

```sh


Table AGENDA borrado.

```