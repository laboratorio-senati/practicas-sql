# Solucionario - Exists y No Exists

## Ejercicios propuestos 

# 1.Elimine las tablas:

```sql
drop table inscriptos;
drop table socios;

```
# 2.Cree las tablas:

```sql
create table socios(
    numero number(4),
    documento char(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key (numero)
);
 
create table inscriptos (
    numerosocio number(4) not null,
    deporte varchar(20) not null,
    cuotas number(2) default 0,
    constraint CK_inscriptos_cuotas
    check (cuotas>=0 and cuotas<=10),
    primary key(numerosocio,deporte),
    constraint FK_inscriptos_socio
    foreign key (numerosocio)
    references socios(numero)
    on delete cascade
);

```
# 3.Ingrese algunos registros:

```sql
insert into socios values(1,'23333333','Alberto Paredes','Colon 111');
insert into socios values(2,'24444444','Carlos Conte','Sarmiento 755');
insert into socios values(3,'25555555','Fabian Fuentes','Caseros 987');
insert into socios values(4,'26666666','Hector Lopez','Sucre 344');
insert into inscriptos values(1,'tenis',1);
insert into inscriptos values(1,'basquet',2);
insert into inscriptos values(1,'natacion',1);
insert into inscriptos values(2,'tenis',9);
insert into inscriptos values(2,'natacion',1);
insert into inscriptos values(2,'basquet',default);
insert into inscriptos values(2,'futbol',2);
insert into inscriptos values(3,'tenis',8);
insert into inscriptos values(3,'basquet',9);
insert into inscriptos values(3,'natacion',0);
insert into inscriptos values(4,'basquet',10);

```
# 4.Se necesita un listado de todos los socios que incluya nombre y domicilio, la cantidad de deportes a los cuales se ha inscripto, empleando subconsulta.

```sql
SELECT s.nombre, s.domicilio, (SELECT COUNT(*) FROM inscriptos WHERE numero = s.numero) AS cantidad_deportes FROM socios s;

```
# 5.Se necesita el nombre de todos los socios, el total de cuotas que debe pagar (10 por cada deporte) y el total de cuotas pagas, empleando subconsulta.

```sql
SELECT s.nombre, (SELECT COUNT(*) FROM deportes) * 10 AS cuotas, (SELECT SUM(cuotas) FROM pagos WHERE numero = s.numero) AS cuotas FROM socios s;
```
# 6.Obtenga la misma salida anterior empleando join.

```sql
SELECT s.nombre, COUNT(DISTINCT d.codigo) * 10 AS cuotas, SUM(p.cuotas) AS cuotas FROM socios s LEFT JOIN inscriptos  ON s.numero = i.numero LEFT JOIN deportes  ON i.codigodeporte = d.codigo LEFT JOIN pagos  ON s.numero = p.numero GROUP BY s.nombre;
```
