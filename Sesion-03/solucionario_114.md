# Solucionario - Disparadores (habilitar y deshabilitar)

## Ejercicios propuestos

## 1.Elimine la tabla "libros" y la tabla "control":

```sql
drop table libros;
drop table control;

```
## 2.Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);

```
## 3.Ingrese algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
## 4.Cree un disparador que se active cuando modificamos algún campo de "libros" y almacene en "control" el nombre del usuario que realiza la actualización, la fecha y en "operacion" coloque el nombre del campo actualizado

```sql

```
## 5.Cree otro desencadenador que se active cuando ingresamos un nuevo registro en "libros", debe almacenar en "control" el nombre del usuario que realiza el ingreso, la fecha e "insercion" en "operacion"

```sql

```
## 6.Cree un tercer trigger sobre "libros" que se active cuando eliminamos un registro de "libros", debe almacenar en "control" el nombre del usuario que realiza la eliminación, la fecha y "borrado" en "operacion"

```sql

```
## 7.Los tres triggers están habilitados. Consultamos el diccionario "user_triggers" para corroborarlo

```sql

```
## 8.Ingrese un libro y compruebe que el trigger "tr_ingresar_libros" se dispara recuperando los registros de "control"

```sql

```
## 9.Deshabilite el trigger "tr_ingresar_libros"

```sql

```
## 10.Consulte el diccionario "user_triggers" para corroborarlo El trigger "tr_ingresar_libros" está deshabilitado, "tr_actualizar_libros" y "tr_eliminar_libros" están habilitados.

```sql

```
## 11.Ingrese un libro y compruebe que el trigger de inserción no se dispara recuperando los registros de "control":

```sql
insert into libros values(152,'El anillo del hechicero','Gaskin','Planeta',22);
select *from control;

```
## 12.Actualice la editorial de varios libros y compruebe que el trigger de actualización se dispara recuperando los registros de "control"

```sql

```
## 13.Deshabilite el trigger "tr_actualizar_libros"

```sql

```
## 14.Consulte el diccionario "user_triggers" para corroborarlo Los triggers "tr_ingresar_libros" y "tr_actualizar_libros" están deshabilitados, "tr_eliminar_libros" está habilitado.

```sql

```
## 15.Borre un libro de "libros" y compruebe que el trigger de borrado se disparó recuperando los registros de "control"

```sql

```
## 16.Deshabilite el trigger "tr_eliminar_libros"

```sql

```
## 17.Consulte el diccionario "user_triggers" para comprobarlo Los tres trigger establecidos sobre "empleados" están deshabilitados.

```sql

```
## 18.Elimine un libro de "libros" y compruebe que tal registro se eliminó de "libros" pero que el trigger de borrado no se dispara recuperando los registros de "control"

```sql

```
## 19.Habilite el trigger "tr_actualizar_libros"

```sql

```
## 20.Actualice el autor de un libro y compruebe que el trigger de actualización se dispara recuperando los registros de "control"

```sql

```
## 21.Habilite todos los triggers establecidos sobre "libros"

```sql

```
## 22.Consulte el diccionario "user_triggers" para comprobar que el estado (status) de todos los triggers establecidos sobre "libros" es habilitado
Los tres trigger establecidos sobre "libros" han sido habilitados. Se activarán ante cualquier sentencia "insert", "update" y "delete".

```sql
