# Solucionario - Alterar secuencia (alter sequence)

## Ejercicios propuestos

## 1. Elimine la tabla "empleados":

```sql
drop table empleados;
```

## 2. Cree la tabla:

```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```

## 3. Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (210), valor inicial (206), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.

```sql
CREATE SEQUENCE sec_legajoempleados
    MINVALUE 1
    MAXVALUE 210
    START WITH 206
    INCREMENT BY 2
    NOCYCLE;

    ALTER SEQUENCE sec_legajoempleados START WITH 206;

```
## 4. Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria.

```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');
```

## 5. Recupere los registros de "libros" para ver los valores de clave primaria.

```sql
select *from all_sequences where sequence_name='SEC_CODIGOLIBROS';
```
## 6. Vea el valor actual de la secuencia empleando la tabla "dual"

```sql
SELECT sec_legajoempleados.CURRVAL FROM dual;

```
## 7. Intente ingresar un registro empleando "nextval":

```sql
insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');
```

Oracle muestra un mensaje de error indicando que la secuencia ha llegado a su valor máximo.

## 8. Altere la secuencia modificando el atributo "maxvalue" a 999.

```sql
ALTER SEQUENCE sec_legajoempleados MAXVALUE 999;

```
## 9. Obtenga información de la secuencia.

```sql
seect * from empleados;
```
## 10. Ingrese el registro del punto 7.

```sql
insert into empleados
values (sec_legajoempleados.nextval,'27632856','lopes');
```
## 11. Recupere los registros.

```sql
selet * from empleados;
```
## 12. Modifique la secuencia para que sus valores se incrementen en 1.

```sql
ALTER SEQUENCE sec_legajoempleados INCREMENT BY 1;

```
## 13. Ingrese un nuevo registro:

```sql
insert into empleados
values (sec_legajoempleados.nextval,'26777888','Federico Fuentes');
```

## 14. Recupere los registros.

```sql
select * from empleados;
```
## 15. Elimine la secuencia creada.

```sql
drop table empleados;
```
## 16. Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.

```sql
select * from empleados;
```