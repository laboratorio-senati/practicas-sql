# Solucionario - Restricciones: validación y estados (validate - novalidate - enable - disable)

# Ejercicios propuestos

## 1. Elimine la tabla:

```sql
drop table empleados;
```

## 2. Créela con la siguiente estructura e ingrese los registros siguientes:

```sql
create table empleados (
    codigo number(6),
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2)
);

insert into empleados
values (1,'22222222','Alberto Acosta','Sistemas',-10);

insert into empleados
values (2,'33333333','Beatriz Benitez','Recursos',3000);

insert into empleados
values (3,'34444444','Carlos Caseres','Contaduria',4000);
```

## 3. Intente agregar una restricción "check" para asegurarse que no se ingresen valores negativos para el sueldo sin especificar validación ni estado:

```sql
alter table empleados
add constraint CK_empleados_sueldo_positivo
check (sueldo>=0);
```
## 4. Vuelva a intentarlo agregando la opción "novalidate".

```sql
alter table empleados
add constraint CK_empleados_sueldo_positivo
novalidate (sueldo>=0);
```
## 5. Intente ingresar un valor negativo para sueldo.

```sql
insert into empleados values (3,'34444444','Carlos Caseres','Contaduria',-400);
```
## 6. Deshabilite la restricción e ingrese el registro anterior.

```sql
ALTER TABLE empleados DISABLE CONSTRAINT ck_empleados;

```
## 7. Intente establecer una restricción "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contaduría" sin especificar validación:

```sql
alter table empleados
add constraint CK_empleados_seccion_lista
check (seccion in ('Sistemas','Administracion','Contaduria'));
```

No lo permite porque existe un valor fuera de la lista.

## 8. Establezca la restricción anterior evitando que se controlen los datos existentes.

```sql
ALTER TABLE empleados
ADD CONSTRAINT ck_empleados_seccion_lista CHECK (sueldo <= 5000) NOVALIDATE;

```
## 9. Vea si las restricciones de la tabla están o no habilitadas y validadas. Muestra 2 filas, una por cada restricción; ambas son de control, ninguna valida los datos existentes, "CK_empleados_sueldo_positivo" está deshabilitada, la otra habilitada.

```sql
SELECT constraint_name, constraint_type, status, validated FROM user_constraints WHERE table_name = 'empleados';

```
## 10. Habilite la restricción deshabilitada. Note que existe un sueldo que infringe la condición.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT sueldo;

```
## 11. Intente modificar la sección del empleado "Carlos Caseres" a "Recursos" No lo permite.

```sql
ALTER TABLE empleados ENABLE CONSTRAINT recursos;

```
## 12. Deshabilite la restricción para poder realizar la actualización del punto precedente.

```sql
ALTER TABLE empleados DISABLE CONSTRAINT ck_empleados;

```
## 13. Agregue una restricción "primary key" para el campo "codigo" deshabilitada.

```sql
ALTER TABLE empleados
ADD CONSTRAINT ck_empleados PRIMARY KEY (codigo)
DISABLE;

```
## 14. Ingrese un registro con código existente.

```sql
insert into empleados values (1,'22222222','Alberto Acosta','Sistemas',-10);

```
## 15. Intente habilitar la restricción. No se permite porque aun cuando se especifica que no lo haga, Oracle verifica los datos existentes, y existe un código repetido.

```sql
ALTER TABLE empleados
ENABLE CONSTRAINT ck_empleados;

```
## 16. Modifique el registro con clave primaria repetida.

```sql
UPDATE empleados SET codigo = codigo WHERE codigo = documeto;

```
## 17. Habilite la restricción "primary key"

```sql
ALTER TABLE empleados
ENABLE CONSTRAINT pk_codigo;

```
## 18. Agregue una restricción "unique" para el campo "documento"

```sql
ALTER TABLE empleados
ADD CONSTRAINT ck_empleados UNIQUE (documento);

```
## 19. Vea todas las restricciones de la tabla "empleados" Muestra las 4 restricciones: 2 de control (1 habilitada y la otra no, no validan datos existentes), 1 "primary key" (habilitada y no valida datos existentes) y 1 única (habilitada y valida los datos anteriores).

```sql
SELECT * status, validated
FROM empleados
WHERE codigo = 'empleados';

```
## 20. Deshabilite todas las restricciones de "empleados"

```sql
ALTER TABLE empleados
DISABLE CONSTRAINT ALL;

```
## 21. Ingrese un registro que viole todas las restricciones.

```sql
insert into empleados
values (22,'33333333765',null,'Recursos',null);
```
## 22. Habilite la restricción "CK_empleados_sueldo_positivo" sin validar los datos existentes.

```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT CK_empleados_sueldo_positivo;

```
## 23. Habilite la restricción "CK_empleados_seccion_lista" sin validar los datos existentes.

```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT CK_empleados_seccion_lista;

```
## 24. Intente habilitar la restricción "PK_empleados_codigo" sin validar los datos existentes.

```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT CK_empleados_codigo;
```
## 25. Intente habilitar la restricción "UQ_empleados_documento" sin validar los datos existentes.

```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT UQ_empleados_documento;
```
## 26. Elimine el registro que infringe con las restricciones "primary key" y "unique".

```sql
DELETE FROM empleados
WHERE codigo = 'valor_del_codigo';

DELETE FROM empleados
WHERE documento = 'valor_del_documento';

```
## 27. Habilite las restricciones "PK_empleados_codigo" y "UQ_empleados_documento" sin validar los datos existentes.

```sql
ALTER TABLE empleados
ENABLE CONSTRAINT PK_empleados_codigo;

ALTER TABLE empleados
ENABLE CONSTRAINT UQ_empleados_documento;

```
## 28. Consulte el catálogo "user_constraints" y analice la información.

```sql
SELECT codigo, documento, nombre, seccion, sueldo
FROM user_constraints;

```
