# Solucionario - Valores nulos (null)

## Ejercicios propuestos

# Ejercicio 01

## 1.crea una tabla con la siguiente estructura:
codio sql:

```sql

 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);

```
salida sql:

```sh

Table MEDICAMENTOS creado.
```
## 2.Visualice la estructura de la tabla "medicamentos" note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".
codio sql:

```sql

describe medicamentos;
```
salida sql:

```sh

Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3) 
```
## 3.Ingrese algunos registros con valores "null" para los campos que lo admitan:
codio sql:

```sql

insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
## 4.Vea todos los registros.
codio sql:

```sql

select * from medicamentos; 
```
salida sql:

```sh

CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                            100
         2 Sertal compuesto                             8,9        150
         3 Buscapina            Roche                              200
```
## 5.Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.
codio sql:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Sertal gotas',null,0,50); 

```
salida sql:

```sh
1 fila insertadas.
```
## 6.Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)
codio sql:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'',null,0,50); 

```
salida sql:

```sh
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("HR"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 7.Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
codio sql:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Sertal gotas',null,null,null); 

```
salida sql:

```sh
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("HR"."MEDICAMENTOS"."CANTIDAD")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
# Ejercicio 02

## 1.Elimine la tabla:
codio sql:

```sql
drop table medicamentos;

```
salida sql:

```sh
Table MEDICAMENTOS borrado.

```
## 2.Créela con la siguiente estructura:
codio sql:

```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);

```
salida sql:

```sh
Table PELICULAS creado.
```
## 3.Visualice la estructura de la tabla.
codio sql:

```sql
describe peliculas;
```
salida sql:

```sh
Table PELICULAS creado.

Nombre   ¿Nulo?   Tipo         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3)    

```
## 4.Ingrese los siguientes registros:
codio sql:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);

```
salida sql:

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
## 5.Recupere todos los registros para ver cómo Oracle los almacenó.
codio sql:

```sql
select * from peliculas;
```
salida sql:

```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal                                   180
         3 Harry Potter y la camara secreta         Daniel R.                      
         0 Mision imposible 2                                                   150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts            0

6 filas seleccionadas. 

```
## 6.Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)
codio sql:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(0,null,'',150);

```
salida sql:

```sh
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("HR"."PELICULAS"."TITULO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 7.Muestre todos los registros.
codio sql:

```sql
select * from peliculas;
```
salida sql:

```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal                                   180
         3 Harry Potter y la camara secreta         Daniel R.                      
         0 Mision imposible 2                                                   150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts            0
         0 null imposible 2                                                     150

7 filas seleccionadas. 

```
