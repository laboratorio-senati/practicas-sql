# Solucionario -  Tipos de datos numéricos

## Ejercicios propuestros

# Propuesto 01

## 1.Elimine la tabla "cuentas":
codigo sql:

```sql
drop table cuentas;

```
## 2.Cree la tabla eligiendo el tipo de dato adecuado para almacenar los datos descriptos arriba:

Número de cuenta: entero hasta 9999, no nulo, no puede haber valores repetidos, clave primaria;


Documento del propietario de la cuenta: cadena de caracteres de 8 de longitud (siempre 8), no nulo;


Nombre del propietario de la cuenta: cadena de caracteres de 30 de longitud,


Saldo de la cuenta: valores que no superan 999999.99

codigo sql:

```sql
create table cuentas(
    numero number(4) not null,
    documento char(8),
    nombre varchar2(30),
    saldo number(8,2),
    primary key (numero)
);

```
## 3.Ingrese los siguientes registros:
codigo sql:

```sql
insert into cuentas(numero,documento,nombre,saldo) values('1234','25666777','Pedro Perez',500000.60);
insert into cuentas(numero,documento,nombre,saldo) values('2234','27888999','Juan Lopez',-250000);
insert into cuentas(numero,documento,nombre,saldo) values('3344','27888999','Juan Lopez',4000.50);
insert into cuentas(numero,documento,nombre,saldo) values('3346','32111222','Susana Molina',1000);

```
## 4.Seleccione todos los registros cuyo saldo sea mayor a "4000" (2 registros)
codigo sql:

```sql
select saldo, nombre from cuentas where saldo >=4000;
```
salida sql:

```sh
     SALDO NOMBRE                        
---------- ------------------------------
  500000,6 Pedro Perez                   
    4000,5 Juan Lopez                    

```
## 5.Muestre el número de cuenta y saldo de todas las cuentas cuyo propietario sea "Juan Lopez" (2 registros)
codigo sql:

```sql
select numero, saldo from cuentas where nombre ='Juan Lopez';
```
## 6.Muestre las cuentas con saldo negativo (1 registro)
codigo sql:

```sql
select saldo from cuentas where saldo < 0;
```
## 7.Muestre todas las cuentas cuyo número es igual o mayor a "3000" (2 registros)
codigo sql:

```sql
select numero, saldo from cuentas where numero >= 3000;
```
# Ejercicio 02

## 1.Elimine la tabla:
codigo sql:

```sql
drop table empleados;

```
## 2.Cree la tabla eligiendo el tipo de dato adecuado para cada campo:
codigo sql:

```sql
create table empleados(
    nombre varchar2(30),
    documento char(8),
    sexo char(1),
    domicilio varchar2(30),
    sueldobasico number(7,2),--máximo estimado 99999.99
    cantidadhijos number(2)--no superará los 99
);

```
## 3.Ingrese algunos registros:
codigo sql:

```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Juan Perez','22333444','m','Sarmiento 123',500,2);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Ana Acosta','24555666','f','Colon 134',850,0);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Bartolome Barrios','27888999','m','Urquiza 479',10000.80,4);

```
## 4.Ingrese un valor de "sueldobasico" con más decimales que los definidos (redondea los decimales al valor más cercano 800.89)
codigo sql:

```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Juan ','22493444','m','Sarmiento 123',800.89,4);
;
```
## 5.Intente ingresar un sueldo que supere los 7 dígitos (no lo permite)
codigo sql:

```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Juan ','22493444','m','Sarmiento 123',8030.8989,4);
;
```
## 6.Muestre todos los empleados cuyo sueldo no supere los 900 pesos
codigo sql:

```sql
select sueldobasico from empleados where sueldobasico <=900;
```
## 7.Seleccione los nombres de los empleados que tengan hijos (3 registros)
codigo sql:

```sql
select nombre, cantidadhijos from empleados where cantidadhijos =3;
```

