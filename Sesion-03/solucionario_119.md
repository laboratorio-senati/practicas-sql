# Solucionario - Permiso de conexión

## Practica de laboratorio

## 1.Elimine el usuario "director", porque si existe, aparecerá un mensaje de error:

```sql
drop user director cascade;

```
## 2.Cree un usuario "director", con contraseña "dire" y 100M de espacio en "system"

```sql

```
## 3.Elimine el usuario "profesor":

```sql
drop user profesor cascade;

```
## 4.Cree un usuario "profesor", con contraseña "profe" y espacio en "system"

```sql

```
## 5.Elimine el usuario "alumno" y luego créelo con contraseña "alu" y espacio en "system"

```sql

```
## 6.Consulte el diccionario "dba_users" y analice la información que nos muestra Deben aparecer los tres usuarios creados anteriormente.

```sql

```
## 7.Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a nuestros tres usuarios Nos muestra que estos usuarios no tienen ningún privilegio concedido.

```sql

```
## 8.Conceda a "director" permiso para conectarse

```sql

```
## 9.Conceda a "profesor" permiso para conectarse

```sql

```
## 10.Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a nuestros 3 usuarios

```sql

```
## 11.Abra una nueva conexión para "director". Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (director)

```sql

```
## 12.En la conexión de "director" consulte sus privilegios

```sql

```
## 13.Obtenga el nombre del usuario conectado

```sql

```
## 14.Vuelva a la conexión "system" (la otra solapa) y compruebe el usuario actual

```sql

```
## 15.Intente abrir una nueva conexión para el usuario inexistente. Debe aparecer un mensaje de error y denegarse la conexión. Cancele.

```sql

```

## 16.Intente abrir una nueva conexión para el usuario "profesor" colocando una contraseña incorrecta. Debe aparecer un mensaje de error y denegarse la conexión. Cancele.

```sql

```
## 17.Abra una nueva conexión para "profesor" colocando los datos correctos. Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (profesor).

```sql

```
## 18.Intentemos abrir una nueva conexión para el usuario "alumno", el cual no tiene permiso. Un mensaje de error indica que el usuario "alumno" no tiene permiso "create session" por lo cual se deniega la conexión. Cancele.

```sql

```
## 19.Conceda a "alumno" permiso de conexión

```sql

```
## 20.Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a "alumno"

```sql

```
## 21.Abra una nueva conexión para "ALUMNO". Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (profesor)

```sql

```
## 22.Consulte el diccionario "user_sys_privs"

```sql

```
## 23.Compruebe que está en la sesión de "alumno"

```sql

```
