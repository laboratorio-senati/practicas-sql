# Solucionario - Recuperar algunos campos (select)

## Ejercicios propuestos

# EJERCICIO 01

## 1.Elimine la tabla, si existe:

codigo sql:

```sql

drop table peliculas;

```
salida sql:

```sh

Table PELICULAS borrado.
```
## 2.Cree la tabla:

codigo sql:

```sql

create table peliculas(
  titulo varchar(20),
  actor varchar(20),
  duracion integer,
  cantidad integer
);

```
salida sql:

```sh

Table PELICULAS creado.
```
## 3.Vea la estructura de la tabla:

codigo sql:

```sql

describe peliculas;

```
salida sql:

```sh

Nombre   ¿Nulo? Tipo         
-------- ------ ------------ 
TITULO          VARCHAR2(20) 
ACTOR           VARCHAR2(20) 
DURACION        NUMBER(38)   
CANTIDAD        NUMBER(38)   
```
## 4.Ingrese los siguientes registros:

codigo sql:

```sql

 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,2);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',90,2);

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


```
## 5.Realice un "select" mostrando solamente el título y actor de todas las películas:
codigo sql:

```sql

select titulo,actor from peliculas;

```
salida sql:

```sh

TITULO               ACTOR               
-------------------- --------------------
Mision imposible     Tom Cruise          
Mision imposible 2   Tom Cruise          
Mujer bonita         Julia R.            
Elsa y Fred          China Zorrilla      
```
## 6.Muestre el título y duración de todas las peliculas.

codigo sql:

```sql

 select titulo,duracion from peliculas;
```
salida sql:

```sh

TITULO                 DURACION
-------------------- ----------
Mision imposible            120
Mision imposible 2          180
Mujer bonita                 90
Elsa y Fred                  90
```
## 7.Muestre el título y la cantidad de copias.

codigo sql:

```sql

select titulo,cantidad from peliculas;
```
salida sql:

```sh

TITULO                 CANTIDAD
-------------------- ----------
Mision imposible              3
Mision imposible 2            2
Mujer bonita                  3
Elsa y Fred                   2
```