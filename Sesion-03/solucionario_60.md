# Solucionario - Restricciones foreign key deshabilitar y validar

# Ejercicios propuestos

## 1. Elimine las tablas "clientes" y "provincias":

```sql
drop table clientes;
drop table provincias;
```

## 2. Créelas con las siguientes estructuras:

```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);
```

## 3. Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Garcia Juan','Sucre 345','Cordoba',1);
insert into clientes values(103,'Lopez Susana','Caseros 998','Posadas',3);
insert into clientes values(104,'Marcelo Moreno','Peru 876','Viedma',4);
insert into clientes values(105,'Lopez Sergio','Avellaneda 333','La Plata',5);
```

## 4. Intente agregar una restricción "foreign key" para que los códigos de provincia de "clientes" existan en "provincias" sin especificar la opción de comprobación de datos

```sql
ALTER TABLE clientes ADD CONSTRAINT fk_clientes_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);
```

## 5. Agregue la restricción anterior pero deshabilitando la comprobación de datos existentes

```sql
ALTER TABLE clientes WITH NOCHECK ADD CONSTRAINT fk_clientes_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);
```

## 6. Vea las restricciones de "clientes"

```sql
SHOW CREATE TABLE clientes;
```
## 7. Deshabilite la restricción "foreign key" de "clientes"

```sql
ALTER TABLE clientes NOCHECK CONSTRAINT codigoprovinsia;
```
## 8. Vea las restricciones de "clientes"

```sql
select * from clientes
```
## 9. Agregue un registro que no cumpla la restricción "foreign key"

```sql
insert into clientes values(1074,'Marcelo Moreno','Peru 876','Viedma',444);

```
Se permite porque la restricción está deshabilitada.

## 10. Modifique el código de provincia del cliente código 104 por 9 Oracle lo permite porque la restricción está deshabilitada.

```sql
UPDATE clientes SET codigoprovincia = 9 WHERE codigo = 104;
```
## 11. Habilite la restricción "foreign key"

```sql
ALTER TABLE clientes CHECK CONSTRAINT codigo;
```
## 12. Intente modificar un código de provincia existente por uno inexistente.

```sql
UPDATE clientes SET codigoprovincia = 2 WHERE codigo = 8;
```
## 13. Intentealterar la restricción "foreign key" para que valide los datos existentes

```sql
ALTER TABLE clientes WITH CHECK CHECK CONSTRAINT codigo;
```
