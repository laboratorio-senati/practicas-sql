# Solucionario - Clave primaria (primary key)

## Ejercicios propuestros

# Propuesto 01

## 1.Elimine la tabla:
codigo sql:

```sql
drop table libros;

```
## 2.Créela con los siguientes campos, estableciendo como clave primaria el campo "codigo":
codigo sql:

```sql
create table libros(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    autor varchar2(20),
    editorial varchar2(15),
    primary key (codigo)
);

```
## 3.Ingrese los siguientes registros:
codigo sql:

```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
insert into libros (codigo,titulo,autor,editorial) values (2,'Martin Fierro','Jose Hernandez','Planeta');
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');

```
## 4.Ingrese un registro con código repetido (aparece un mensaje de error)
codigo sql:

```sql
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');
```
salida sql:

```sh
Error que empieza en la línea: 1 del comando :
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo')
Informe de error -
ORA-00001: restricción única (HR.SYS_C008407) violada
```
## 5.Intente ingresar el valor "null" en el campo "codigo"
codigo sql:

```sql
insert into libros (codigo,titulo,autor,editorial) values (null,'Aprenda PHP','Mario Molina','Nuevo Siglo');
```
salida sql:

```sh
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("HR"."LIBROS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 6.Intente actualizar el código del libro "Martin Fierro" a "1" (mensaje de error)
codigo sql:

```sql
update libros set codigo = ´1´ where titulo = 'Martin Fierro';
```
salida sql:
```sh
Error SQL: ORA-00911: carácter no válido
00911. 00000 -  "invalid character"
```
## 7.Actualice el código del libro "Martin Fierro" a "10"
codigo sql:

```sql
update libros set titulo = 'Martin Fierro'  where codigo = ´10´;
```
## 8.Vea qué campo de la tabla "LIBROS" fue establecido como clave primaria
codigo sql:

```sql
select * FROM libros WHERE codigo = 'PRIMARY';
```

# Ejercicio 02

## 1.Elimine la tabla "alumnos":
codigo sql:

```sql
drop table alumnos;

```
salida sql:

```sh

```
## 2.Cree la tabla con la siguiente estructura intentando establecer 2 campos como clave primaria, el campo "documento" y "legajo":
codigo sql:

```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
);

```
salida sql:

```sh
Informe de error -
ORA-02260: la tabla sólo puede tener una clave primaria
02260. 00000 -  "table can have only one primary key"
*Cause:    Self-evident.
*Action:   Remove the extra primary key.
```
## 3.Cree la tabla estableciendo como clave primaria el campo "documento":
codigo sql:

```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

```
salida sql:

```sh
Table ALUMNOS creado.
```
## 4.Verifique que el campo "documento" no admite valores nulos
codigo sql:

```sql
SELECT documento FROM alumnos WHERE documento = 'tabla' AND documento = 'campo';
```
## 5.Ingrese los siguientes registros:
codigo sql:

```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345','Perez Mariana','Colon 234');
insert into alumnos (legajo,documento,nombre,domicilio) values('A567','23545345','Morales Marcos','Avellaneda 348');

```
salida sql:

```sh
1 fila insertadas.


1 fila insertadas.

```
## 6.Intente ingresar un alumno con número de documento existente (no lo permite)
codigo sql:

```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345','Lian pedro','Con 534');

```
salida sql:

```sh
Error que empieza en la línea: 1 del comando :
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345','Lian pedro','Con 534')
Informe de error -
ORA-00001: restricción única (HR.SYS_C008415) violada
```
## 7.Intente ingresar un alumno con documento nulo (no lo permite)
codigo sql:

```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233',null,'Li jose','Con 536');

```
salida sql:

```sh
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("HR"."ALUMNOS"."DOCUMENTO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
