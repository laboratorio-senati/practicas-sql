# Solucionario - Privilegios del sistema (conceder)

## Ejercicios propuestos

## 1.Cree un usuario denominado "director", con contraseña "escuela", asignándole 100M de espacio en "system" (100M). Antes elimínelo por si existe:

```sql

```
## 2.Intente iniciar una sesión como "director". No es posible, no hemos concedido el permiso correspondiente. Aparece un mensaje indicando que el usuario "director" no tiene permiso "create session" por lo tanto no puede conectarse.

```sql

```
## 3.Vea los permisos de "director" No tiene ningún permiso.

```sql

```
## 4.Conceda a "director" permiso para iniciar sesion y para crear tablas

```sql

```
## 5.Vea los permisos de "director" Tiene permiso "create session" y para crear tablas.

```sql

```
## 6.Inicie una sesión como "director".

```sql

```
## 7.Como "administrador", elimine los usuarios "profesor" y "alumno", por si existen

```sql

```
## 8.Cree un usuario denominado "profesor", con contraseña "maestro", asigne espacio en "system" (100M)

```sql

```
## 9.Cree un usuario denominado "estudiante", con contraseña "alumno" y tablespace "system" (no asigne "quota")

```sql

```
## 10.Consulte el diccionario de datos correspondiente para ver si existen los 3 usuarios creados

```sql

```
## 11.Conceda a "profesor" y a "estudiante" permiso para conectarse

```sql

```
## 12.Conceda a "estudiante" permiso para crear tablas

```sql

```
## 13.Consulte el diccionario de datos "sys_privs" para ver los permisos de los 3 usuarios creados "director" y "estudiante" tienen permisos para conectarse y para crear tablas, "profesor" tiene permiso para conectarse.

```sql

```
## 14.Retome su sesión como "director" y cree una tabla:

```sql
 create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );

```
## 15.Inicie una sesión como "profesor" e intente crear una tabla:

```sql
 create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );

```
## 16.Consulte los permisos de "profesor" No tiene permiso para crear tablas, únicamente para crear sesión.

```sql

```
## 17.Cambie a la conexión de administrador y conceda a "profesor" permiso para crear tablas

```sql

```
## 18.Cambie a la sesión de "profesor" y cree una tabla Ahora si podemos hacerlo, "profesor" tiene permiso "create table".

```sql

```
## 19.Consulte nuevamente los permisos de "profesor" Tiene permiso para crear tablas y para crear sesión.

```sql

```
## 20.Inicie una sesión como "estudiante" e intente crear una tabla:

```sql
 create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );

```
## 21.Vuelva a la conexión de "administrador" y consulte todas las tablas denominadas "PRUEBA"
Note que hay una tabla propiedad de "director" y otra que pertenece a "profesor".

```sql

```