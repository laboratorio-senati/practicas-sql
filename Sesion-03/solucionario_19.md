# Solucionario - Alias (encabezados de columnas)

## Ejercicios propuestros

## 1.Elimine la tabla:
codigo sql:

```sql
drop table articulos;

```
## 2.Cree la tabla:
codigo sql:

```sql
create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);

```
## 3.Ingrese algunos registros:
codigo sql:

```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);

```
## 4.El comercio hace un descuento del 15% en ventas mayoristas. Necesitamos recuperar el código, nombre, decripción de todos los artículos con una columna extra que muestre el precio de cada artículo para la venta mayorista con el siguiente encabezado "precio mayorista"
codigo sql:

```sql
SELECT Codigo, Nombre, Descripcion, Precio AS 'precio mayorista' FROM Articulos
```
## 5.Muestre los precios de todos los artículos, concatenando el nombre y la descripción con el encabezado "artículo" (sin emplear "as" ni comillas)
codigo sql:

```sql
select precio, nombre, descripcion from articulos;
```
## 6.Muestre todos los campos de los artículos y un campo extra, con el encabezado "monto total" en la que calcule el monto total en dinero de cada artículo (precio por cantidad)
codigo sql:

```sql
SELECT Codigo, Nombre, Descripcion, Precio, Cantidad, (Precio * Cantidad) AS 'monto total' FROM Articulos;
```
## 7.Muestre la descripción de todas las impresoras junto al precio con un 20% de recargo con un encabezado que lo especifique.
codigo sql:

```sql
SELECT Descripcion, (Precio * 1.2) AS 'Precio con recargo'
FROM Articulos
WHERE Descripcion LIKE '%impresora%';

```

