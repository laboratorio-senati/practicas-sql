# Solucionario - Disparador de inserción a nivel de fila (insertar gatillo para cada fila)

## Ejercicios propuestos

# 1.Elimine las tablas:

```sql
drop table empleados;
drop table control;

```
# 2.Cree las tablas con las siguientes estructuras:

```sql
create table empleados(
    documento char(8),
    apellido varchar2(30),
    nombre varchar2(30),
    seccion varchar2(20)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
# 3.Cree un disparador que se dispare una vez por cada registro ingresado en "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "insert" sobre "empleados"

```sql
create or replace trigger tr_ingresar_empleados before insert on empleados for each row begin insert into Control values(user,sysdate); end tr_ingresar_empleados;
```
# 4.Vea qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado

```sql
select *from user_triggers where trigger_name ='TR_INGRESAR_EMPLEADOS';

```
# 5.Ingrese algunos registros en "empleados":

```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria');
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria');
insert into empleados values('22999000','FUENTES','Federico','Sistemas');
insert into empleados values('22555666','CASEROS','Carlos','Contaduria');
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas');
insert into empleados values('23666777','JUAREZ','Juan','Contaduria');

```
# 6.Verifique que el trigger se disparó 6 veces, una por cada fila afectada en la sentencia "insert" anteriormente ejecutada; consultamos la tabla "control":

```sql
select *from control;

```
