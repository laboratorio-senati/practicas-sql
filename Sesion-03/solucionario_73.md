# Solucionario - Subconsultas con in

## Ejercicios propuestos 

# 1.Elimine las tablas "clientes" y "ciudades":

```sql
drop table clientes;
drop table ciudades;

```
# 2.Cree la tabla "clientes" (codigo, nombre, domicilio, ciudad, codigociudad) y "ciudades" (codigo, nombre). Agregue una restricción "primary key" para el campo "codigo" de ambas tablas y una "foreing key" para validar que el campo "codigociudad" exista en "ciudades" con eliminación en cascada:

```sql
create table ciudades(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);

create table clientes (
    codigo number(4),
    nombre varchar2(30),
    domicilio varchar2(30),
    codigociudad number(2) not null,
    primary key(codigo),
    constraint FK_clientes_ciudad
    foreign key (codigociudad)
    references ciudades(codigo)
    on delete cascade
);

```
# 3.Ingrese algunos registros para ambas tablas:

```sql
insert into ciudades values(1,'Cordoba');
insert into ciudades values(2,'Cruz del Eje');
insert into ciudades values(3,'Carlos Paz');
insert into ciudades values(4,'La Falda');
insert into ciudades values(5,'Villa Maria');
insert into clientes values (100,'Lopez Marcos','Colon 111',1);
insert into clientes values (101,'Lopez Hector','San Martin 222',1);
insert into clientes values (105,'Perez Ana','San Martin 333',2);
insert into clientes values (106,'Garcia Juan','Rivadavia 444',3);
insert into clientes values (107,'Perez Luis','Sarmiento 555',3);
insert into clientes values (110,'Gomez Ines','San Martin 666',4);
insert into clientes values (111,'Torres Fabiola','Alem 777',5);
insert into clientes values (112,'Garcia Luis','Sucre 888',5);

```
# 4.Necesitamos conocer los nombres de las ciudades de aquellos clientes cuyo domicilio es en calle "San Martin", empleando subconsulta.

```sql
SELECT nombre FROM ciudades WHERE ciudades,codigociudad IN (SELECT clientes.codigociudad FROM clientes WHERE clientes.domicilio LIKE 'San Martin');
```
# 5.Obtenga la misma salida anterior pero empleando join.

```sql
SELECT nombre FROM ciudades JOIN clientes ON codigociudad = clientes.codigociudad WHERE clientes.domicilio LIKE 'San Martin';
```
# 6.Obtenga los nombre de las ciudades de los clientes cuyo apellido no comienza con una letra específica (letra "G"), empleando subconsulta.

```sql
SELECT ciudades.nombre FROM ciudades WHERE ciudades.codigo IN (SELECT clientes.codigociudad FROM clientes WHERE clientes.apellido NOT LIKE 'G');
```
# 7.Pruebe la subconsulta del punto 6 separada de la consulta exterior para verificar que retorna una lista de valores de un solo campo.

```sql
SELECT clientes.codigociudad FROM clientes WHERE clientes.apellido NOT LIKE 'G';
```
