# Solucionario -  Agregar campos (alter table-add)

# Ejercicios propuesto

Trabaje con una tabla llamada "empleados".

## 1. Elimine la tabla y créela:

```sql
drop table empleados;

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30)
);
```

## 2. Agregue un campo "fechaingreso" de tipo date que acepte valores nulos

```sql
ALTER TABLE empleados ADD fechaingreso DATE NULL;
```
## 3. Verifique que la estructura de la tabla ha cambiado

```sql
Verifique que la estructura de la tabla ha cambiado
```
## 4. Agregue un campo "seccion" de tipo caracter que no permita valores nulos y verifique que el nuevo campo existe

```sql
ALTER TABLE empleados ADD seccion CHAR(10) NOT NULL;
```
## 5. Ingrese algunos registros:

```sql
insert into empleados values('Lopez','Juan','Colon 123','10/10/1980','Contaduria');
insert into empleados values('Gonzalez','Juana','Avellaneda 222','01/05/1990','Sistemas');
insert into empleados values('Perez','Luis','Caseros 987','12/09/2000','Secretaria');
```

## 6. Intente agregar un campo "sueldo" que no admita valores nulos.

```sql
UPDATE empleados SET sueldo = 0 WHERE sueldo IS NULL;
```
## 7. Agregue el campo "sueldo" no nulo y con el valor 0 por defecto.

```sql
ALTER TABLE empleados ADD sueldo DECIMAL(10, 2) NOT NULL DEFAULT 0;
```
## 8. Verifique que la estructura de la tabla ha cambiado.

```sql
desc empleados;
```