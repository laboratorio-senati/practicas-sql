# Solucionario - Actualizar registros (update)

## Ejercicios propuestos

# Ejercicio 01

## 1.crée una tabla con la siguiente estructura:
codio sql:

```sql

drop table agenda;

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
salida sql:

```sh

Table AGENDA creado.
```
## 2.Ingrese los siguientes registros:
codio sql:

```sql

insert into agenda (apellido,nombre,domicilio,telefono) values ('Acosta','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Suarez','Susana','Gral. Paz 1234','4123456');

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
## 3.Modifique el registro cuyo nombre sea "Juan" por "Juan Jose" (1 registro actualizado)
codio sql:

```sql

update agenda set nombre = 'Juan Jose' where nombre = 'Juan';
```
salida sql:

```sh

1 fila actualizadas.
```
## 4.Actualice los registros cuyo número telefónico sea igual a "4545454" por "4445566" (2 registros)
codio sql:

```sql

update agenda set telefono = '4445566' where  telefono = '4545454';
```
salida sql:

```sh

2 filas actualizadas.
```
## 5.Elimine la tabla
codio sql:

```sql

drop table agenda;
```
salida sql:

```sh

Table AGENDA borrado.
```