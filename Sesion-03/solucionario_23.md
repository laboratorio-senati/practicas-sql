# Solucionario - Ordenar registros (order by)

## Ejercicios propuestros

## 1.Elimine la tabla "visitas" y créela con la siguiente estructura:
codigo sql:

```sql
drop table visitas;

create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);

```
## 2.Ingrese algunos registros:
codigo sql:

```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina',to_date('2020/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@hotmail.com>','Chile',to_date('2020/02/13 11:08:10', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/02 14:12:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/06/17 20:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/02/08 20:05:40', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/06 18:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2019/10/05 23:00:00', 'yyyy/mm/dd hh24:mi:ss'));

```
## 3.Ordene los registros por fecha, en orden descendente.
codigo sql:

```sql
select * from visitas order by fecha desc;
```
## 4.Muestre el nombre del usuario, pais y el mes, ordenado por pais (ascendente) y el mes (descendente)
codigo sql:

```sql
select nombre, pais, fecha from visitas order by pais asc, fecha desc;
```
## 5.Muestre los mail, país, ordenado por país, de todos los que visitaron la página en octubre (4 registros)
codigo sql:

```sql
SELECT mail, pais FROM Visitas WHERE(fecha) = 10 ORDER BY pais;
```