# Solucionario - Disparador de insercion a nivel de sentencia

## Ejercicios propuestos

# 1.Eliminar las tres tablas:

```sql
drop table libros;
drop table ofertas;
drop table control;

```
# 2.Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table ofertas(
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
# 3.Establezca el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
 alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

```
# 4.Cree un disparador que se dispare cuando se ingrese un nuevo registro en "ofertas"; el disparador debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "insertar" sobre "ofertas"

```sql
create or replace trigger tr_ingresar_ofertas before insert on ofertas begin insert into Control values(user,sysdate); end tr_ingresar_ofertas;
```
# 5.Vea qué informa el diccionario "user_triggers" con respecto al disparador creado anteriormente

```sql
select *from user_triggers where trigger_name ='TR_INGRESAR_OFERTAS';

```
# 6.Ingrese algunos registros en "libros"

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(105,'El aleph','Borges','Emece',32);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);

```
# 7.Ingrese en "ofertas" los libros de "libros" cuyo precio no supera los $30, utilizando la siguiente sentencia:

```sql
insert into ofertas select titulo,autor,precio from libros where precio<30;

```
# 8.Verifique que el gatillo se disparó consultando la tabla "control".

```sql
select *from control;

```
