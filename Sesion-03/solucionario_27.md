# Solucionario - Búsqueda de patrones (like - not like)

## Ejercicios propuestos

## 1.Elimine la tabla:
codigo sql

```sql
drop table empleados;

```
## 2.Cree la tabla:
codigo sql

```sql
create table empleados(
    nombre varchar2(30),
    documento char(8) not null,
    domicilio varchar2(30),
    fechaingreso date,
    seccion varchar2(20),
    sueldo number(6,2),
    primary key(documento)
);

```
## 3.Ingrese algunos registros:
codigo sql

```sql
insert into empleados
values('Juan Perez','22333444','Colon 123','08/10/1990','Gerencia',900.50);

insert into empleados
values('Ana Acosta','23444555','Caseros 987','18/12/1995','Secretaria',590.30);

insert into empleados
values('Lucas Duarte','25666777','Sucre 235','15/05/2005','Sistemas',790);

insert into empleados
values('Pamela Gonzalez','26777888','Sarmiento 873','12/02/1999','Secretaria',550);

insert into empleados
values('Marcos Juarez','30000111','Rivadavia 801','22/09/2002','Contaduria',630.70);

insert into empleados
values('Yolanda perez','35111222','Colon 180','08/10/1990','Administracion',400);

insert into empleados
values('Rodolfo perez','35555888','Coronel Olmedo 588','28/05/1990','Sistemas',800);

```
## 4.Muestre todos los empleados con apellido "Perez" empleando el operador "like" (1 registro) Note que no incluye los "perez" (que comienzan con minúscula).
codigo sql

```sql
SELECT * FROM Empleados WHERE Apellido LIKE 'Perez' AND Apellido NOT LIKE 'perez%';
```
## 5.Muestre todos los empleados cuyo domicilio comience con "Co" y tengan un "8" (2 registros)
codigo sql

```sql
SELECT * FROM Empleados WHERE Domicilio LIKE 'Co%' AND Domicilio LIKE '%8%';
```
## 6.Seleccione todos los empleados cuyo documento finalice en 0 ó 4 (2 registros)
codigo sql

```sql
SELECT * FROM empleados WHERE documento LIKE '0' OR documento LIKE '4';

```
## 7.Seleccione todos los empleados cuyo documento NO comience con 2 y cuyo nombre finalice en "ez" (1 registro)
codigo sql

```sql
SELECT * FROM Empleados WHERE Documento NOT LIKE '2%' AND Nombre LIKE '%ez';

```
## 8.Recupere todos los nombres que tengan una "G" o una "J" en su nombre o apellido (3 registros)
codigo sql

```sql
SELECT nombre FROM empleados WHERE nombre LIKE '%G%' OR nombre LIKE '%J%' OR apellido LIKE '%G%' OR apellido LIKE '%J%';

```
## 9.Muestre los nombres y sección de los empleados que pertenecen a secciones que comiencen con "S" o "G" y tengan 8 caracteres (3 registros)
codigo sql

```sql
SELECT nombre, seccion FROM empleados WHERE (seccion LIKE 'S' OR seccion LIKE 'G') AND (seccion) = 8;

```
## 10.Muestre los nombres y sección de los empleados que pertenecen a secciones que NO comiencen con "S" (3 registros)
codigo sql

```sql
SELECT nombre, seccion FROM empleados WHERE seccion NOT LIKE 'S%';

```
## 11.Muestre todos los nombres y sueldos de los empleados cuyos sueldos se encuentren entre 500,00 y 599,99 empleando "like" (2 registros)
codigo sql

```sql
SELECT nombre, sueldo FROM empleados WHERE sueldo BETWEEN 500.00 AND 599.99;

```
## 12.Muestre los empleados que hayan ingresado en la década del 90 (5 registros)
codigo sql

```sql
SELECT nombre, fechaingreso FROM empleados WHERE(fechaingreso) BETWEEN 1990 AND 1999;

```
## 13.Agregue 50 centavos a todos los sueldos que no tengan centavos (4 registros) y verifique recuperando todos los registros.
codigo sql

```sql
update empleados set sueldo + 50;
```
## 14.Elimine todos los empleados cuyos apellidos comiencen con "p" (minúscula) (2 registros)
codigo sql

```sql
DELETE FROM Empleados WHERE Apellido LIKE 'p%';

```
