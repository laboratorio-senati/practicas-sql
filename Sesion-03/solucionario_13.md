# Solucionario - Vaciar la tabla (truncate table)

## Ejercicio de laboratorio


## Eliminamos la tabla:
codigo sql:

```sql
drop table libros;

```
## Creamos la tabla:
codigo sql:

```sql
create table libros(
    codigo number(4),
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);

```

## Agregamos algunos registros:
codigo sql:

```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

```

## Seleccionamos todos los registros:
codigo sql:

```sql
select *from libros;

```

## Truncamos la tabla:
codigo sql:

```sql
truncate table libros;

```

## Si consultamos la tabla, vemos que aún existe pero ya no tiene registros:
codigo sql:

```sql
select *from libros;

```

## Ingresamos nuevamente algunos registros:
codigo sql:

```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

```

## Eliminemos todos los registros con "delete":
codigo sql:

```sql
delete from libros;

```
