# Solucionario -  Crear tabla a partir de otra (create table-select)

## Ejercicios propuestos 

# 1.Elimine las tablas "empleados" y "sucursales":

```sql
drop table empleados;
drop table sucursales;

```
# 2.Cree la tabla "sucursales":

```sql
create table sucursales( 
    codigo number(4),
    ciudad varchar2(30) not null,
    primary key(codigo)
);

```
# 3.Cree la tabla "empleados":

```sql
create table empleados( 
    documento char(8) not null,
    nombre varchar2(30) not null,
    domicilio varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2),
    codigosucursal number(4),
    primary key(documento),
    constraint FK_empleados_sucursal
    foreign key (codigosucursal)
    references sucursales(codigo)
 );

```
# 4.Ingrese algunos registros para ambas tablas:

```sql
insert into sucursales values(1,'Cordoba');
insert into sucursales values(2,'Villa Maria');
insert into sucursales values(3,'Carlos Paz');
insert into sucursales values(4,'Cruz del Eje');
insert into empleados values('22222222','Ana Acosta','Avellaneda 111','Secretaria',500,1);
insert into empleados values('23333333','Carlos Caseros','Colon 222','Sistemas',800,1);
insert into empleados values('24444444','Diana Dominguez','Dinamarca 333','Secretaria',550,2);
insert into empleados values('25555555','Fabiola Fuentes','Francia 444','Sistemas',750,2);
insert into empleados values('26666666','Gabriela Gonzalez','Guemes 555','Secretaria',580,3);
insert into empleados values('27777777','Juan Juarez','Jujuy 777','Secretaria',500,4);
insert into empleados values('28888888','Luis Lopez','Lules 888','Sistemas',780,4);
insert into empleados values('29999999','Maria Morales','Marina 999','Contaduria',670,4);

```
# 5.Realice un join para mostrar todos los datos de "empleados" incluyendo la ciudad de la sucursal

```sql
SELECT e.*, s.ciudad FROM empleados e JOIN sucursales s ON codigo = codigo;

```
# 6.Cree una tabla llamada "secciones" que contenga las secciones de la empresa (primero elimínela)

```sql
drop table secciones;

create table secciones as
  (select distinct seccion as nombre
   from empleados);

```
# 7.Recupere la información de "secciones"

```sql
select * from secciones;
```
# 8.Se necesita una nueva tabla llamada "sueldosxseccion" que contenga la suma de los sueldos de los empleados por sección (de todas las sucursales). Primero elimine la tabla

```sql
drop table sueldosxseccion;

create table sueldosxseccion as
  (select seccion, sum(sueldo) as total
  from empleados
  group by seccion);

```
# 9.Recupere los registros de la nueva tabla

```sql
  select * from sueldosxseccion;
```
# 10.Se necesita una nueva tabla llamada "sucursalCordoba" que contenga los nombres y sección de los empleados de la ciudad de Córdoba. En primer lugar, eliminamos la tabla. Luego, consulte las tablas "empleados" y "sucursales" y guarde el resultado en la nueva tabla

```sql
DROP TABLE IF EXISTS sucursalCordoba;


CREATE TABLE sucursalCordoba (
  nombre VARCHAR(50),
  seccion VARCHAR(50)
);


```
# 11.Consulte la nueva tabla

```sql
select * from sucursalCordoba;
```