# Solucionario - Disparador de actualización - lista de campos (update trigger)

## Ejercicios propuestos

# 1.Elimine las tablas:

```sql
drop table control;
drop table libros;

```
# 2.Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
# 3.Ingrese algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
# 4.Establezca el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

```
# 5.Cree un desencadenador a nivel de sentencia que se dispare cada vez que se actualicen los campos "precio" y "editorial" ; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "update" sobre "precio" o "editorial" de "libros"

```sql

```
# 6.Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado

```sql

```
# 7.Aumente en un 10% el precio de todos los libros de editorial "Nuevo siglo'

```sql

```
# 8.Vea cuántas veces se disparó el trigger consultando la tabla "control" El trigger se disparó 1 vez.

```sql

```
# 9.Cambie la editorial, de "Planeta" a "Sudamericana"

```sql

```
# 10.Veamos si el trigger se disparó consultando la tabla "control" El trigger se disparó.

```sql

```
# 11.Modifique un campo diferente de los que activan el trigger

```sql

```
# 12.Verifique que el cambio se realizó

```sql

```
# 13.Verifique que el trigger no se disparó El trigger no se disparó (no hay nuevas filas en "control"), pues está definido únicamente sobre los campos "precio" y "editorial".

```sql

```


