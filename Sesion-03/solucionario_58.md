# Solucionario - Restricciones foreign key en la misma tabla

# Ejercicios propuestos

Una empresa registra los datos de sus clientes en una tabla llamada "clientes". Dicha tabla contiene un campo que hace referencia al cliente que lo recomendó denominado "referenciadopor". Si un cliente no ha sido referenciado por ningún otro cliente, tal campo almacena "null".

## 1. Elimine la tabla y créela:

```sql
drop table clientes;
 
create table clientes(
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    referenciadopor number(5),
    primary key(codigo)
);
```

## 2. Ingresamos algunos registros:

```sql
insert into clientes values (50,'Juan Perez','Sucre 123','Cordoba',null);
insert into clientes values(90,'Marta Juarez','Colon 345','Carlos Paz',null);
insert into clientes values(110,'Fabian Torres','San Martin 987','Cordoba',50);
insert into clientes values(125,'Susana Garcia','Colon 122','Carlos Paz',90);
insert into clientes values(140,'Ana Herrero','Colon 890','Carlos Paz',9);
```

## 3. Intente agregar una restricción "foreign key" para evitar que en el campo "referenciadopor" se ingrese un valor de código de cliente que no exista.

```sql
insert into clientes values(140,'Ana Herrero','Colon 890','Carlos Paz',9);
```


## 4. Cambie el valor inválido de "referenciadopor" del registro que viola la restricción por uno válido:

```sql
update clientes set referenciadopor=90 where referenciadopor=9;
```

## 5. Agregue la restricción "foreign key" que intentó agregar en el punto 3

```sql
ALTER TABLE clientes ADD CONSTRAINT fk_clientes_codigo FOREIGN KEY (codigo) REFERENCES provincias(codigo);

```
## 6. Vea la información referente a las restricciones de la tabla "clientes"

```sql
SHOW CREATE TABLE clientes;
```
La tabla tiene 2 restricciones.

## 7. Intente agregar un registro que infrinja la restricción.

```sql
insert into clientes values(140,'rero','Colon 990','Carlos Paz',999);
```
## 8. Intente modificar el código de un cliente que está referenciado en "referenciadopor"

```sql
UPDATE clientes SET codigo = 9 WHERE codigo = 12;
```
## 9. Intente eliminar un cliente que sea referenciado por otro en "referenciadopor"

```sql
DELETE FROM clientes WHERE codigo = 9;
```
## 10. Cambie el valor de código de un cliente que no referenció a nadie.

```sql
UPDATE clientes SET codigo = 3 WHERE codigo = 12;
```
