# Solucionario - Ingresar algunos campos

## Ejercicios propuestros

# Ejercicios propuestos

## 1.Elimine la tabla "cuentas":
codigo sql:

```sql
drop table cuentas;

```
## 2.Cree la tabla :
codigo sql:

```sql
create table cuentas(
    numero number(10) not null,
    documento char(8) not null,
    nombre varchar2(30),
    saldo number(9,2)
);

```
## 3.Ingrese un registro con valores para todos sus campos, omitiendo la lista de campos.
codigo sql:

```sql
insert into cuentas (numero,documento,nombre,saldo) values ('1234567891','HDGISDLH','JUAN LOPEZ','12,662');
```
## 4.Ingrese un registro omitiendo algún campo que admita valores nulos.
codigo sql:

```sql
insert into cuentas (numero,documento,nombre,saldo) values ('1234567891','HDGISDLH',NULL,NULL);
```
## 5.Verifique que en tal campo se almacenó "null"
codigo sql:

```sql
SELECT * FROM CUENTAS;
```
## 6.Intente ingresar un registro listando 3 campos y colocando 4 valores. Un mensaje indica que hay demasiados valores.
codigo sql:

```sql
INSERT INTO CUENTAS (NUMERO,DOCUMEMTO,NOMBRE) VALUES ('135155','HSFVUS','LIAN LOPEZ','237,9');
```
## 7.Intente ingresar un registro listando 3 campos y colocando 2 valores. Un mensaje indica que no hay suficientes valores.
codigo sql:

```sql
INSERT INTO CUENTAS (NUMERO,DOCUMEMTO,NOMBRE) VALUES ('135155','HSFVUS');
```
## 8.Intente ingresar un registro sin valor para un campo definido "not null".
codigo sql:

```sql
INSERT INTO CUENTAS (NUMERO,DOCUMEMTO,NOMBRE,SALDO) VALUES ('HSFVUS','LIAN LOPEZ','237,9');
```
## 9.Vea los registros ingresados.
codigo sql:

```sql
SELECT * FROM CUENTAS;
```
