# Solucionario - Control de flujo (loop)

## Ejercicios propuestos 

# 1.Elimine la tabla y créela con la siguiente estructura:

```sql
 drop table empleados;
 create table empleados(
  nombre varchar2(40),
  sueldo number(6,2)
 );

```
# 2.Ingrese algunos registros:

```sql
 insert into empleados values('Acosta Ana',550); 
 insert into empleados values('Bustos Bernardo',850); 
 insert into empleados values('Caseros Carolina',900); 
 insert into empleados values('Dominguez Daniel',490); 
 insert into empleados values('Fuentes Fabiola',820); 
 insert into empleados values('Gomez Gaston',740); 
 insert into empleados values('Huerta Hernan',1050); 

```
# 3.Muestre la suma total de todos los sueldos realizando un "select" (5400)

```sql
 select sum(sueldo) from empleados;

```
# 4.Se necesita incrementar los sueldos en forma proporcional, en un 10% cada vez y controlar que la suma total de sueldos no sea menor a $7000, si lo es, el bucle debe continuar y volver a incrementar los sueldos, en caso de superarlo, se saldrá del ciclo repetitivo; es decir, este bucle continuará el incremento de sueldos hasta que la suma de los mismos llegue o supere los 7000.

```sql
 declare total number; begin loop  update empleados set sueldo=sueldo+(sueldo*0.1);  select sum(sueldo) into total from empleados;  exit when total>7000;      end loop; end;
```
# 5.Verifique que los sueldos han sido incrementados y la suma de todos los sueldos es superior a 7000

```sql
select sueldo from empleados;
select sum(sueldo) from empleados;
```
# 6.Muestre el sueldo máximo realizando un "select"

```sql
 select max(sueldo) from empleados;

```
# 7.Se necesita incrementar los sueldos en forma proporcional, en un 5% cada vez y controlar que el sueldo máximo alcance o supere los $1600, al llegar o superarlo, el bucle debe finalizar. Incluya una variable contador que cuente cuántas veces se repite el bucle

```sql
 set serveroutput on; execute dbms_output.enable (20000); declare maximo number; contador number:=0; begin loop update empleados set sueldo=sueldo+(sueldo*0.05); contador:=contador+1; select max(sueldo) into maximo from empleados; exit when maximo>1600; end loop;  dbms_output.put_line(contador); end;
```
# 8.Verifique que los sueldos han sido incrementados y el sueldo máximo es igual o superior a 1600

```sql
select sueldo from empleados;
 select max(sueldo) from empleados;
```
# 9.Muestre el sueldo mínimo realizando un "select"

```sql
 select min(sueldo) from empleados;

```
# 10.Se necesita incrementar los sueldos en forma proporcional, en un 10% cada vez y controlar que el sueldo mínimo no supere los $900. Emplee la sintaxis "if CONDICION then exit"

```sql
 declare minimo number; begin loop select min(sueldo) into minimo from empleados; if (minimo+minimo*0.1>900) then exit; else
 update empleados set sueldo=sueldo+(sueldo*0.1); end if;  end loop; end;
```
# 11.Muestre el sueldo mínimo realizando un "select"

```sql
 select min(sueldo) from empleados;

```