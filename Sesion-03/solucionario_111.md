# Solucionario - Disparador (old y new)

## Ejercicios propuestos

## 1.Elimine las tablas:

```sql
drop table libros;
drop table ofertas;

```
## 2.Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar(20),
    precio number(6,2)
);

create table ofertas(
    codigo number(6),
    precio number(6,2)
);

```
## 3.Ingrese algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
## 4.Cree un trigger a nivel de fila que se dispare al ingresar un registro en "libros"; si alguno de los libros ingresados tiene un precio menor o igual a $30 debe ingresarlo en "ofertas"

```sql

```
## 5.Ingrese un libro en "libros" cuyo precio sea inferior a $30

```sql

```
## 6.Verifique que el trigger se disparó consultando "ofertas"

```sql

```
## 7.Ingrese un libro en "libros" cuyo precio supere los $30

```sql

```
## 8.Verifique que no se ingresó ningún registro en "ofertas"

```sql

```
## 9.Cree un trigger a nivel de fila que se dispare al modificar el precio de un libro. Si tal libro existe en "ofertas" y su nuevo precio ahora es superior a $30, debe eliminarse de "ofertas"; si tal libro no existe en "ofertas" y su nuevo precio ahora es inferior a $30, debe agregarse a "ofertas"

```sql

```
## 10.Aumente a más de $30 el precio de un libro que se encuentra en "ofertas"

```sql

```
## 11.Verifique que el trigger se disparó consultando "libros" y "ofertas"

```sql

```
## 12.Disminuya a menos de $31 el precio de un libro que no se encuentra en "ofertas"

```sql

```
## 13.Verifique que el trigger se disparó consultando "libros" y "ofertas"

```sql

```
## 14.Aumente el precio de un libro que no se encuentra en "ofertas"

```sql

```
## 15.Verifique que el trigger se disparó pero no se modificó "ofertas"

```sql

```
## 16.Cree un trigger a nivel de fila que se dispare al borrar un registro en "libros"; si alguno de los libros eliminados está en "ofertas", también debe eliminarse de dicha tabla.

```sql

```
## 17.Elimine un libro en "libros" que esté en "ofertas"

```sql

```
## 18.Verifique que el trigger se disparó consultando "libros" y "ofertas"

```sql

```
## 19.Elimine un libro en "libros" que No esté en "ofertas"

```sql

```
## 20.Verifique que el trigger se disparó consultando "libros" y "ofertas"

```sql

```
## 21.Cree una tabla llamada "control" que almacene el código, la fecha y el precio de un libro, antes elimínela por si existe

```sql

```
## 22.Cree un disparador que se dispare cada vez que se actualice el precio de un libro; el trigger debe ingresar en la tabla "control", el código del libro cuyo precio se actualizó, la fecha y el precio anterior.

```sql

```
## 23.Actualice el precio de un libro

```sql

```
## 24.Controle que el precio se ha modificado en "libros" y que se agregó un registro en "control"

```sql

```
## 25.Modifique nuevamente el precio del libro cambiado en el punto 11

```sql

```
## 26.Controle que el precio se ha modificado en "libros" y que se agregó un nuevo registro en "control"

```sql

```
## 27.Modifique el precio de varios libros en una sola sentencia que incluya al modificado anteriormente

```sql

```
## 28.Controle que el precio se ha modificado en "libros" y que se agregó un nuevo registro en "control"

```sql

```
## 29.Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado

```sql

```