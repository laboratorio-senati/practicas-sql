# Solucionario - Agregar campos y restricciones (alter table)

# Ejercicios propuestos

Trabaje con una tabla llamada "empleados".
## 1. Elimine la tabla y créela:

```sql
drop table empleados;

create table empleados(
    documento char(8) not null,
    nombre varchar2(10),
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Buenos Aires'
);
```

## 2. Agregue el campo "legajo" de tipo number(3) y una restricción "primary key"

```sql
ALTER TABLE empleados ADD COLUMN legajo number(3);

ALTER TABLE empleados ADD CONSTRAINT pk_legajo PRIMARY KEY (legajo);
```
## 3. Vea si la estructura cambió y si se agregó la restricción

```sql
desc empleados;
```
## 4. Agregue el campo "hijos" de tipo number(2) y en la misma sentencia una restricción "check" que no permita valores superiores a 30

```sql
ALTER TABLE empleados ADD COLUMN hijos number(2) CONSTRAINT CK_empleados_hijos CHECK (hijos <= 30);
```
## 5. Ingrese algunos registros:

```sql
insert into empleados values('22222222','Juan Lopez','Colon 123','Cordoba',100,2);
insert into empleados values('23333333','Ana Garcia','Sucre 435','Cordoba',200,3);
```

## 6. Intente agregar el campo "sueldo" de tipo number(6,2) no nulo y una restricción "check" que no permita valores negativos para dicho campo.

```sql
ALTER TABLE empleados ADD COLUMN sueldo number(6,2) NOT NULL CONSTRAINT CK_empleados_sueldo CHECK (sueldo >= 0);
```
No lo permite porque no damos un valor por defecto para dicho campo no nulo y los registros existentes necesitan cargar un valor.

## 7. Agregue el campo "sueldo" de tipo number(6,2) no nulo, con el valor por defecto 0 y una restricción "check" que no permita valores negativos para dicho campo

```sql
ALTER TABLE empleados ADD COLUMN sueldo number(6,2) NOT NULL DEFAULT 0 CONSTRAINT CK_empleados_sueldo CHECK (sueldo >= 0);
```
## 8. Recupere los registros

```sql
select *from empleados;
```

## 9. Vea la nueva estructura de la tabla

```sql
desc empleados;
```
## 10. Vea las restricciones

```sql
sp_helpconstraint empleados;
```