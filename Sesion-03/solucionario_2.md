# solucionario - Ingresar registros (insert into- select)

## ejercicios propuestos

# Ejercicio 01

## 1.Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)

codigo sql:

```sql

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
salida sql

```sh

Table AGENDA creado.


```
## 2.Visualice la estructura de la tabla "agenda" (describe)

codigo sql

```sql

describe agenda;

```
salida sql:

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 

```
## 3.Ingrese los siguientes registros:

codigo sql:

```sql

insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.
```
## 4.Seleccione todos los registros de la tabla.

codigo sql:

```sql
SELECT * FROM agenda;
```
salida sql:

```sh

APELLIDO                       NOMBRE               DOMICILIO                      TELEFONO   
------------------------------ -------------------- ------------------------------ -----------
Moreno                         Alberto              Colon 123                      4234567    
Torres                         Juan                 Avellaneda 135                 4458787 

```
## 5.Elimine la tabla "agenda"

codigo sql:

```sql
drop TABLE agenda;
```
salida sql:
```sh
Table AGENDA borrado.
```
## 6.Intente eliminar la tabla nuevamente (aparece un mensaje de error)

codigo sql:
```sql

drop table agenda;
```
salida sql:

```sh

Error que empieza en la línea: 14 del comando :
drop table agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
# Ejercicio 02

## 1.Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)

codigo sql:

```sql

create table libros (
  titulo varchar(20),
  autor varchar(30),
  editorial varchar(15)
);
```
salida sql:

```sh

Table LIBROS creado.
```

## 2.Visualice la estructura de la tabla "libros"

codigo sql

```sql

describe libros;
```
salida sql:

```sh

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 
```

## 3.Ingrese los siguientes registros:

codigo sql

```sql

insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

## 4.Muestre todos los registros (select) de "libros"

codigo sql

```sql

select * from libros;
```
salida sql:

```sh

TITULO               AUTOR                          EDITORIAL      
-------------------- ------------------------------ ---------------
El aleph             Borges                         Planeta        
Martin Fierro        Jose Hernandez                 Emece          
Aprenda PHP          Mario Molina                   Emece          
```
## 5.elimine la tabla libros

codigo sql

```sql

drop table libros
```
salida sql:

```sh

Table LIBROS borrado.
```