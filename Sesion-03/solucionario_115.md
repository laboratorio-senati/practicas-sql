# Solucionario - Disparador (eliminar)

## Ejercicios propuestos

## 1.Elimine las tablas:

```sql
drop table ventas;
drop table articulos;

```
## 2.Cree las tablas con las siguientes estructuras:

```sql
create table articulos(
    codigo number(4) not null,
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4),
    constraint PK_articulos_codigo
    primary key (codigo)
);

create table ventas(
    codigo number(4),
    cantidad number(4),
    fecha date,
    constraint FK_ventas_articulos
    foreign key (codigo)
    references articulos(codigo)
);

```
## 3.Cree una secuencia llamada "sec_codigoart", estableciendo que comience en 1, sus valores estén entre 1 y 9999 y se incrementen en 1. Antes elimínela por si existe.

```sql

```
## 4.Active el paquete para permitir mostrar salida en pantalla.

```sql

```
## 5.Cree un trigger que coloque el siguiente valor de una secuencia para el código de "articulos" cada vez que se ingrese un nuevo artículo. Podemos ingresar un nuevo registro en "articulos" sin incluir el código porque lo ingresará el disparador luego de calcularlo. Si al ingresar un registro en "articulos" incluimos un valor para código, será ignorado y reemplazado por el valor calculado por el disparador.

```sql

```
## 6.Ingrese algunos registros en "articulos" sin incluir el código:

```sql
insert into articulos (descripcion, precio, stock) values ('cuaderno rayado 24h',4.5,100);
insert into articulos (descripcion, precio, stock) values ('cuaderno liso 12h',3.5,150);
insert into articulos (descripcion, precio, stock) values ('lapices color x6',8.4,60);

```
## 7.Recupere todos los artículos para ver cómo se almacenó el código

```sql

```
## 8.Ingrese algunos registros en "articulos" incluyendo el código:

```sql
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas metal',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);

```
## 9.Recupere todos los artículos para ver cómo se almacenó los códigos Ignora los códigos especificados ingresando el siguiente de la secuencia.

```sql

```
## 10.Cuando se ingresa un registro en "ventas", se debe:

controlar que el código del artículo exista en "articulos" (lo hacemos con la restricción "foreign key" establecida en "ventas");


controlar que exista stock, lo cual no puede controlarse con una restricción "foreign key" porque el campo "stock" no es clave primaria en la tabla "articulos"; cree un trigger. Si existe stock, debe disminuirse en "articulos".


Cree un trigger a nivel de fila sobre la tabla "ventas" para el evento se inserción. Cada vez que se realiza un "insert" sobre "ventas", el disparador se ejecuta. El disparador controla que la cantidad que se intenta vender sea menor o igual al stock del articulo y actualiza el campo "stock" de "articulos", restando al valor anterior la cantidad vendida. Si la cantidad supera el stock, debe producirse un error, revertirse la acción y mostrar un mensaje




```sql

```
## 11.Ingrese un registro en "ventas" cuyo código no exista en "articulos" Aparece un mensaje de error, porque el código no existe. El trigger se ejecutó.

```sql

```
## 12.Verifique que no se ha agregado ningún registro en "ventas"

```sql

```
## 13.Ingrese un registro en "ventas" cuyo código exista en "articulos" y del cual haya suficiente stock Note que el trigger se disparó, aparece el texto "tr_insertar_ventas activado".

```sql

```
## 14.Verifique que el trigger se disparó consultando la tabla "articulos" (debe haberse disminuido el stock) y se agregó un registro en "ventas"

```sql

```
## 15.Ingrese un registro en "ventas" cuyo código exista en "articulos" y del cual NO haya suficiente stock Aparece el mensaje mensaje de error 20001 y el texto que muestra que se disparó el trigger.

```sql

```
## 16.Verifique que NO se ha disminuido el stock en "articulos" ni se ha agregado un registro en "ventas"

```sql

```
## 17.El comercio quiere que se realicen las ventas de lunes a viernes de 8 a 18 hs. Reemplace el trigger creado anteriormente "tr_insertar_ventas" para que No permita que se realicen ventas fuera de los días y horarios especificados y muestre un mensaje de error

```sql

```
## 18.Ingrese un registro en "ventas", un día y horario permitido, si es necesario, modifique la fecha y la hora del sistema

```sql

```
## 19.Verifique que se ha agregado un registro en "ventas" y se ha disminuido el stock en "articulos"

```sql

```
## 20.Ingrese un registro en "ventas", un día permitido fuera del horario permitido (si es necesario, modifique la fecha y hora del sistema) Se muestra un mensaje de error.

```sql

```
## 21.Ingrese un registro en "ventas", un día sábado a las 15 hs.

```sql

```
## 22.El comercio quiere que los registros de la tabla "articulos" puedan ser ingresados, modificados y/o eliminados únicamente los sábados de 8 a 12 hs. Cree un trigger "tr_articulos" que No permita que se realicen inserciones, actualizaciones ni eliminaciones en "articulos" fuera del horario especificado los días sábados, mostrando un mensaje de error. Recuerde que al ingresar un registro en "ventas", se actualiza el "stock" en "articulos"; el trigger debe permitir las actualizaciones del campo "stock" en "articulos" de lunes a viernes de 8 a 18 hs. (horario de ventas)

```sql

```
## 23.Ingrese un nuevo artículo un sábado a las 9 AM Note que se activan 2 triggers.

```sql

```
## 24.Elimine un artículo, un sábado a las 16 hs. Mensaje de error.

```sql

```
## 25.Actualice el precio de un artículo, un domingo

```sql

```
## 26.Actualice el precio de un artículo, un lunes en horario de ventas Mensaje de error.

```sql

```
## 27.Ingrese un registro en "ventas" que modifique el "stock" en "articulos", un martes entre las 8 y 18 hs. Note que se activan 2 triggers.

```sql

```
## 28.Consulte el diccionario "user_triggers" para ver cuántos trigger están asociados a "articulos" y a "ventas" (3 triggers)

```sql

```
## 29.Elimine el trigger asociado a "ventas"

```sql

```
## 30.Elimine las tablas "ventas" y "articulos"

```sql

```
## 31.Consulte el diccionario "user_triggers" para verificar que al eliminar la tabla "articulos" se han eliminado todos los triggers asociados a ella

```sql

```
