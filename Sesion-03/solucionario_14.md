# Solucionario - Tipos de datos alfanuméricos

## Ejercicios propuestros

# Propuesto 01

## 1.Elimine la tabla "autos"
codigo sql:

```sql
drop table autos;
```
## 2.Cree la tabla eligiendo el tipo de dato adecuado para cada campo, estableciendo el campo "patente" como clave primaria:
codigo sql:

```sql
create table autos(
    patente char(6),
    marca varchar2(20),
    modelo char(4),
    precio number(8,2),
    primary key (patente)
);

```
## 3.Ingrese los siguientes registros:
codigo sql:

```sql
insert into autos (patente,marca,modelo,precio) values('ABC123','Fiat 128','1970',15000);
insert into autos (patente,marca,modelo,precio) values('BCD456','Renault 11','1990',40000);
insert into autos (patente,marca,modelo,precio) values('CDE789','Peugeot 505','1990',80000);
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane','1998',95000);

```
## 4.Ingrese un registro omitiendo las comillas en el valor de "modelo" Oracle convierte el valor a cadena.
codigo sql:

```sql
insert into autos (patente,marca,modelo,precio) values('DEFH12','Renat Mene',1998,95000);
```
## 5.Vea cómo se almacenó.
codigo sql:

```sql
SELECT * FROM AUTOS; 
```
## 6.Seleccione todos los autos modelo "1990"
codigo sql:

```sql
SELECT marca, modelo FROM AUTOS WHERE MODELO =1990; ```
salida sql:

```sh
MARCA                MODE
-------------------- ----
Renault 11           1990
Peugeot 505          1990


```
## 7.Intente ingresar un registro con un valor de patente de 7 caracteres
codigo sql:

```sql

insert into autos (patente,marca,modelo,precio) values('DEF012Y','Renaul gane','1988',96000);
```
## 8.Intente ingresar un registro con valor de patente repetida.

codigo sql:

```sql
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane','1998',95000);
```
# Ejercicio 02

## 1.Elimine la tabla "clientes"
codigo sql

```sql
drop table clientes;
```
## 2.Créela eligiendo el tipo de dato más adecuado para cada campo:
codigo sql

```sql
create table clientes(
    documento char(8) not null,
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2 (11)
);

```
## 4.Ingrese algunos registros:
codigo sql

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333444','Perez','Juan','Sarmiento 980','4223344');
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('23444555','Perez','Ana','Colon 234',null);
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634',null);

```
## 5.Intente ingresar un registro con más caracteres que los permitidos para el campo "telefono"
codigo sql

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333242444','Perez','Juan','Sarmiento 980','422334437');

```
## 6.Intente ingresar un registro con más caracteres que los permitidos para el campo "documento"
codigo sql

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('2233344433557','direz','Jian','Sarmieno 980','4223344');

```
## 7.Intente ingresar un registro omitiendo las comillas en el campo "apellido"
codigo sql

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333444',Perez,'Juan','Sarmiento 980','4223344');

```
## 8.Seleccione todos los clientes de apellido "Perez" (2 registros)
codigo sql

```sql
select apellido from clientes where apellido = Perez;
```