# Solucionario - Restricción primary key

## Ejercicios propuestos

# Ejercicio 01

## 1. Elimine la tabla:

```sql
drop table empleados;
```

## 2. Créela con la siguiente estructura:

```sql
create table empleados (
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20)
);
```

## 3. Ingrese algunos registros, dos de ellos con el mismo número de documento:

```sql

insert into empleados(documento,nombre,seccion) values ('40035086','jose','14');
insert into empleados(documento,nombre,seccion) values ('40035086','luis','19');
```
## 4. Intente establecer una restricción "primary key" para la tabla para que el documento no se repita ni admita valores nulos.

```sql
ALTER TABLE empleados
ADD CONSTRAINT pk_empleados_documento PRIMARY KEY (documento);

```
No lo permite porque la tabla contiene datos que no cumplen con la restricción, debemos eliminar (o modificar) el registro que tiene documento duplicado.

## 5. Establecezca la restricción "primary key" del punto 4

```sql
ALTER TABLE empleados
ADD CONSTRAINT pk_empleados PRIMARY KEY (id);

```
## 6. Intente actualizar un documento para que se repita.

```sql

```
No lo permite porque va contra la restricción.

## 7. Intente establecer otra restricción "primary key" con el campo "nombre".

```sql
ALTER TABLE empleados
ADD CONSTRAINT pk_empleados_nombre PRIMARY KEY (nombre);

```
## 8. Vea las restricciones de la tabla "empleados" consultando el catálogo "user_constraints" (1 restricción "P")

```sql
SELECT legado, documento
FROM empleados
WHERE legado = 'empleados' AND documento = 'P';

```
## 9. Consulte el catálogo "user_cons_columns"

```sql
SELECT legado, documento, nombre
FROM empleados
WHERE legado= 'empleados' AND documento= 'empleados_pk';

```
## Ejercicio 02

## 1. Elimine la tabla:

```sql
drop table remis;
```

## 2. Cree la tabla con la siguiente estructura:

```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);
```

## 3. Ingrese algunos registros sin repetir patente y repitiendo número.

```sql
insert into remis(numero,patente,marca,modelo) values ('1','4','13','set');
insert into remis(numero,patente,marca,modelo) values ('2','5','10','get');
insert into remis(numero,patente,marca,modelo) values ('3','2','14','lal');
```
## 4. Ingrese un registro con patente nula.

```sql
insert into remis(numero,patente,marca,modelo) values ('1','null','13','set');
```
## 5. Intente definir una restricción "primary key" para el campo "numero".

```sql
ALTER TABLE remis
ADD CONSTRAINT numero PRIMARY KEY (numero);
```
## 6. Intente establecer una restricción "primary key" para el campo "patente".

```sql
ALTER TABLE remis
ADD CONSTRAINT patente PRIMARY KEY (patenete);
```
## 7. Modifique el valor "null" de la patente.

```sql
UPDATE remis
SET patente = 'nuevo'
WHERE patente IS NULL;

```
## 8. Establezca una restricción "primary key" del punto 6.

```sql
ALTER TABLE remis
ADD CONSTRAINT numero PRIMARY KEY (numero);

```
## 9. Vea la información de las restricciones consultando "user_constraints" (1 restricción "P")

```sql
SELECT numero, patente, marca, modelo
FROM remis
WHERE user_constraints = 'P';

```
