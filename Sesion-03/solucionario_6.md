# Solucionario - Operadores relacionales

## Ejercicios propuestos

# Ejercicio 01

## 1.Cree la tabla, con la siguiente estructura:
codio sql:

```sql

create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(6,2),
    cantidad number(3)
);

```
salida sql:

```sh

Table ARTICULOS creado.
```
## 2.Vea la estructura de la tabla.
codio sql:

```sql

describe articulos;
```
salida sql:

```sh

Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(5)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(6,2)  
CANTIDAD           NUMBER(3)    

```
## 3.Ingrese algunos registros:
codio sql:

```sql

insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
## 4.Seleccione los datos de las impresoras (2 registros)
codio sql:

```sql

select * from articulos where nombre ='impresora';
```
salida sql:

```sh

1	impresora	Epson Stylus C45	400,8	20
2	impresora	Epson Stylus C85	500	30
```
## 5.Seleccione los artículos cuyo precio sea mayor o igual a 400 (3 registros)
codio sql:

```sql

select * from articulos where precio>=400;
```
salida sql:

```sh

1	impresora	Epson Stylus C45	400,8	20
2	impresora	Epson Stylus C85	500	30
3	monitor	    Samsung 14	        800	10
```
## 6.Seleccione el código y nombre de los artículos cuya cantidad sea menor a 30 (2 registros)
codio sql:

```sql

select codigo, nombre from articulos where cantidad<30;
```
salida sql:

```sh

1	impresora
3	monitor
```
## 7.Selecciones el nombre y descripción de los artículos que NO cuesten $100 (4 registros)
codio sql:

```sql

select nombre, descripcion from articulos where precio != 100;
```
salida sql:

```sh

impresora	Epson Stylus C45
impresora	Epson Stylus C85
monitor	    Samsung 14
teclado  	español Biswal
```
## 8.elimine la tabla articulos
codio sql:

```sql

drop table articulos;
```
salida sql:

```sh

Table ARTICULOS borrado.
```