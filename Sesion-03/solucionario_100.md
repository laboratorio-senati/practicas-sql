# Solucionario - Control de flujo (for)

## Ejercicios propuestos 

Con la estructura repetitiva "for... loop" que vaya del 1 al 20, muestre los números pares.

Dentro del ciclo debe haber una estructura condicional que controle que el número sea par y si lo es, lo imprima por pantalla.

Con la estructura repetitiva "for... loop" muestre la sumatoria del número 5; la suma de todos los números del 1 al 5. Al finalizar el ciclo debe mostrarse por pantalla la sumatoria de 5 (15).

Cree una función que reciba un valor entero y retorne el factorial de tal número; el factorial se obtiene multiplicando el valor que recibe por el anterior hasta llegar a multiplicarlo por uno.

Llame a la función creada anteriormente y obtenga el factorial de 5 y de 4 (120 y 24).

Cree un procedimiento que reciba dos parámetros numéricos; el procedimiento debe mostrar la tabla de multiplicar del número enviado como primer argumento, desde el 1 hasta el númeo enviado como segundo argumento. Emplee "for".

Ejecute el procedimiento creado anteriormente enviándole los valores necesarios para que muestre la tabla del 6 hasta el 20.

Ejecute el procedimiento creado anteriormente enviándole los valores necesarios para que muestre la tabla del 9 hasta el 10.