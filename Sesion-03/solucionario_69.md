# Solucionario - Eliminar campos (alter table - drop)

# Ejercicios propuestos

Trabaje con una tabla llamada "empleados" y "secciones".
## 1. Elimine las tablas y créelas:

```sql
drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar(20),
    primary key (codigo)
);

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30),
    seccion number(2),
    sueldo number(8,2),
    constraint CK_empleados_sueldo
    check (sueldo>=0) disable,
    fechaingreso date,
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo)
    on delete set null
 );
```

## 2. Ingrese algunos registros en ambas tablas:

```sql
insert into secciones values(8,'Secretaria');
insert into secciones values(9,'Contaduria');
insert into secciones values(10,'Sistemas');
insert into empleados values('Lopez','Juan','Colon 123',8,505.50,'10/10/1980');
insert into empleados values('Gonzalez','Juana','Avellaneda 222',9,600,'01/05/1990');
insert into empleados values('Perez','Luis','Caseros 987',10,800,'12/09/2000');
```

## 3. Elimine el campo "domicilio" y luego verifique la eliminación

```sql
ALTER TABLE empleados DROP COLUMN domicilio;

DESC empleados;
```
## 4. Vea las restricciones de "empleados" (1 restricción "foreign key" y 2 "check")

```sql
sp_helpconstraint empleados;

DESC empleados;
```
## 5. Intente eliminar el campo "codigo" de "secciones"

```sql
ALTER TABLE secciones DROP COLUMN codigo;
```
## 6. Elimine la restricción "foreign key" de "empleados", luego elimine el campo "codigo" de "secciones" y verifique la eliminación

```sql
ALTER TABLE empleados DROP CONSTRAINT seccion;

ALTER TABLE secciones DROP COLUMN codigo;

DESC secciones;
```
## 7. Verifique que al eliminar el campo "codigo" de "secciones" se ha eliminado la "primary key" de "secciones"

```sql
DESC secciones;
```
## 8. Elimine el campo "sueldo" y verifique que la restricción sobre tal campo se ha eliminado

```sql
ALTER TABLE empleados DROP COLUMN sueldo;
```
## 9. Cree un índice no único por el campo "apellido" y verifique su existencia consultando "user_indexes"

```sql
sp_helpindex empleados;
```
## 10. Elimine el campo "apellido" y verifique que el índice se ha eliminado

```sql
ALTER TABLE empleados DROP COLUMN apellido;

desc empleados;
```
## 11. Elimine 2 campos de "empleados" y vea la estructura de la tabla

```sql
ALTER TABLE empleados DROP COLUMN seccion DROP COLUMN sueldo;
```
## 12. Intente eliminar el único campo de "empleados"

```sql
alter table empleados drop column nombre;
```