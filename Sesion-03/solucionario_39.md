# Solucionario - Restriccioncheck

## Ejercicios propuestos

# Ejercicio 01

## 1. Elimine la tabla:
 
```sql
drop table empleados;
```

## 2. Créela con la siguiente estructura:

```sql
create table empleados (
    documento char(8),
    nombre varchar2(30),
    cantidadhijos number(2),
    seccion varchar2(20),
    sueldo number(6,2) default -1
);
```

## 3. Agregue una restricción "check" para asegurarse que no se ingresen valores negativos para el sueldo.

```sql
alter table empleados
add constraint CK_empleados_sueldo
check (sueldo>=0 and precio>=0);

```
## 4. Intente ingresar un registro con la palabra clave "default" en el campo "sueldo" (mensaje de error)

```sql
insert into empleados values ('22222222','Alberto Lopez',1,'Sistemas',default);
```
## 5. Ingrese algunos registros válidos:

```sql
insert into empleados values ('22222222','Alberto Lopez',1,'Sistemas',1000);
insert into empleados values ('33333333','Beatriz Garcia',2,'Administracion',3000);
insert into empleados values ('34444444','Carlos Caseres',0,'Contaduría',6000);
```

## 6. Intente agregar otra restricción "check" al campo sueldo para asegurar que ninguno supere el valor 5000. La sentencia no se ejecuta porque hay un sueldo que no cumple la restricción.

```sql
ALTER TABLE empleados
ADD CONSTRAINT ck_empleados_sueldo CHECK (sueldo <= 5000);

```
## 7. Elimine el registro infractor y vuelva a crear la restricción

```sql
DELETE FROM empleados
WHERE sueldo > 5000;

```
## 8. Establezca una restricción "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contaduría".

```sql
ALTER TABLE empleados
ADD CONSTRAINT ck_seccion_values CHECK (seccion IN ('Sistemas', 'Administracion', 'Contaduría'));

```
## 9. Ingrese un registro con valor "null" en el campo "seccion".

```sql
insert into empleados values ('222443332','Alberto ',1,null,1000);
```
## 10. Establezca una restricción "check" para "cantidadhijos" que permita solamente valores entre 0 y 15.

```sql
ALTER TABLE nombre_tabla
ADD CONSTRAINT ck_cantidadhijos_range CHECK (cantidadhijos >= 0 AND cantidadhijos <= 15);

```
## 11. Vea todas las restricciones de la tabla (4 filas)

```sql
SELECT documento, nombre, cantidadhijos, seccio, suelfo
FROM information_schema.table_constraints
WHERE empleados = 'empleados';

```
## 12. Intente agregar un registro que vaya contra alguna de las restricciones al campo "sueldo". Mensaje de error porque se infringe la restricción "CK_empleados_sueldo_positivo".

```sql
insert into empleados values ('88944444','brenda lis',9,'Contaduría',null);
```
## 13. Intente modificar un registro colocando en "cantidadhijos" el valor "21".

```sql
update cantidadhojos set cantidadhijos = 21;
```
## 14. Intente modificar el valor de algún registro en el campo "seccion" cambiándolo por uno que no esté incluido en la lista de permitidos.

```sql
update seccion set seccion where seccion = 30; 
```
## 16. Agregue un registro con documento nulo.

```sql
insert into empleados values (null,'spedro',2,'Contaduria',5000);
```
## 17. Intente agregar una restricción "primary key" para el campo "documento". No lo permite porque existe un registro con valor nulo en tal campo.

```sql
UPDATE empleados
SET documento = 'valor_no_nulo'
WHERE documento IS NULL;

```
## 18. Elimine el registro que infringe la restricción y establezca la restricción del punto 17.

```sql
DELETE FROM empleados
WHERE documento IS NULL;

```
## 19. Consulte "user_constraints", mostrando los campos "constraint_name", "constraint_type" y "search_condition" de la tabla "empleados" (5 filas)

```sql
SELECT constraint_name, constraint_type, search_condition
FROM empleados
WHERE empleaddos = 'empleados';

```
## 20. Consulte el catálogo "user_cons_colums" recuperando el nombre de las restricciones establecidas en el campo sueldo de la tabla "empleados" (2 filas)

```sql
SELECT *
FROM user_cons_columns
WHERE sueldo = 'empleados' AND sueldo = 'sueldo';

```
