# Solucionario - Borrar registros (delete)

## Ejercicios propuestos

# Ejercicio 01

## 1.Cree la tabla con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):
codio sql:

```sql

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
salida sql:

```sh

Table AGENDA creado.
```
## 2.Ingrese los siguientes registros (insert into):
codio sql:

```sql

insert into agenda(apellido,nombre,domicilio,telefono) values ('Alvarez','Alberto','Colon 123','4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Salas','Susana','Gral. Paz 1234','4123456');

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
## 3.Elimine el registro cuyo nombre sea "Juan" (1 registro)
codio sql:

```sql

delete from agenda where nombre = 'Juan';
```
salida sql:

```sh

1 fila eliminado
```
## 4.Elimine los registros cuyo número telefónico sea igual a "4545454" (2 registros)
codio sql:

```sql

delete from agenda where telefono = '4545454';
```
salida sql:

```sh

1 fila eliminado


2 filas eliminado
```
## 5.Elimine todos los registros (2 registros)

codio sql:

```sql

delete from agenda;
```
salida sql:

```sh

2 filas eliminado

```
## 6.Elimine la tabla.

codio sql:

```sql

drop table agenda;
```
salida sql:

```sh

Table AGENDA borrado.

```