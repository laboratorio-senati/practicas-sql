# Solucionario - Otros operadores relacionales (between)

## Ejercicios propuestos

# Ejercicio 01

## 1.Elimine la tabla "visitas" y créela con la siguiente estructura:
codigo sql

```sql
drop table visitas;

create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);

```
## 2.Ingrese algunos registros:
codigo sql

```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina','10/10/2016');

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@gotmail.com>','Chile','10/10/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina','11/10/2016');

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico','12/10/2016');

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico','12/09/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@gmail.com>','Argentina','12/09/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina','15/09/2016');

insert into visitas
values ('Federico1','<federicogarcia@xaxamail.com>','Argentina',null);

```
## 3.Seleccione los usuarios que visitaron la página entre el '12/09/2016' y '11/10/2016' (6 registros) Note que incluye los de fecha mayor o igual al valor mínimo y menores o iguales al valor máximo, y que los valores nulos no se incluyen.
codigo sql

```sql
SELECT nombre FROM Visitas WHERE Fecha >= '2016-09-12' AND Fecha <= '2016-10-11' AND Fecha IS NOT NULL;

```

# Ejercicio 02

## 1.Elimine la tabla y créela con la siguiente estructura:
codigo sql

```sql
drop table medicamentos;

create table medicamentos(
    codigo number(6) not null,
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(6,2),
    cantidad number(4),
    fechavencimiento date not null,
    primary key(codigo)
);

```
## 2.Ingrese algunos registros:
codigo sql

```sql
insert into medicamentos
values(102,'Sertal','Roche',5.2,10,'01/02/2020');

insert into medicamentos
values(120,'Buscapina','Roche',4.10,200,'01/12/2017');

insert into medicamentos
values(230,'Amoxidal 500','Bayer',15.60,100,'28/12/2017');

insert into medicamentos
values(250,'Paracetamol 500','Bago',1.90,20,'01/02/2018');

insert into medicamentos
values(350,'Bayaspirina','Bayer',2.10,150,'01/12/2019');

insert into medicamentos
values(456,'Amoxidal jarabe','Bayer',5.10,250,'01/10/2020');

```
## 3.Recupere los nombres y precios de los medicamentos cuyo precio esté entre 5 y 15 (2 registros)
codigo sql

```sql
select nombre, precio from medicamentos where precio between 5 and 15;
```
## 4.Seleccione los registros cuya cantidad se encuentre entre 100 y 200 (3 registros)
codigo sql

```sql
select * from medicamentos where cantidad between 100 and 200;
```
## 5.Recupere los remedios cuyo vencimiento se encuentre entre la fecha actual y '01/01/2028' inclusive.
codigo sql

```sql
select * from medicamentos where fechavencimiento >= current_date and fechavencimiento <= '01/01/2028';
```
## 6.Elimine los remedios cuyo vencimiento se encuentre entre el año 2017 y 2018 inclusive (3 registros)
codigo sql

```sql
delete from medicamentos where fechavencimiento between 2017 and 2018;
```
