# Solucionario - Indices (eliminar)

## Ejercicios propuestos

## 1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table alumnos;

create table alumnos(
    legajo char(5) not null,
    documento char(8) not null,
    nombre varchar2(30),
    curso char(1) not null,
    materia varchar2(20) not null,
    notafinal number(4,2)
);
```

## 2. Cree un índice no único para el campo "nombre".

```sql
CREATE INDEX idx_nombre ON alumnos(nombre);

```
## 3. Establezca una restricción "primary key" para el campo "legajo"

```sql
ALTER TABLE alumnos ADD CONSTRAINT pk_legajo PRIMARY KEY (legajo);

```
## 4. Verifique que se creó un índice con el nombre de la restricción.

```sql
select * from alumnos;
```
## 5. Verifique que se creó un índice único con el nombre de la restricción consultando el diccionario de índices.

```sql
SELECT nombre, legajo
FROM alumnos
WHERE nombre = 'alumnos' AND legajo = 'pk_legajo';

```
## 6. Intente eliminar el índice "PK_alumnos_legajo" con "drop index".

```sql
DROP INDEX PK_alumnos_legajo;

```
## 7. Cree un índice único para el campo "documento".

```sql
CREATE UNIQUE INDEX idx_documento ON alumnos (documento);

```
## 8. Agregue a la tabla una restricción única sobre el campo "documento" y verifique que no se creó un índice, Oracle emplea el índice creado en el punto anterior.

```sql
ALTER TABLE alumnos ADD CONSTRAINT uk_documento UNIQUE (documento);

```
## 9. Intente eliminar el índice "I_alumnos_documento" (no se puede porque una restricción lo está utilizando)

```sql
ALTER TABLE alumnos DROP CONSTRAINT uk_documento;

```
## 10. Elimine la restricción única establecida sobre "documento".

```sql
ALTER TABLE alumnos DROP CONSTRAINT uk_documento;

```
## 11. Verifique que el índice "I_alumnos_documento" aún existe.

```sql
SELECT nombre FROM alumnos WHERE nombre = 'I_alumnos_documento';

```
## 12. Elimine el índice "I_alumnos_documento", ahora puede hacerlo porque no hay restricción que lo utilice.

```sql
DROP INDEX I_alumnos_documento;

```
## 13. Elimine el índice "I_alumnos_nombre".

```sql
DROP INDEX I_alumnos_nombre;

```
## 14. Elimine la restricción "primary key"/

```sql
ALTER TABLE alumnos DROP CONSTRAINT PK_alumnos_legajo;

```
## 15. Verifique que el índice "PK_alumnos_legajo" fue eliminado (porque fue creado por Oracle al establecerse la restricción)

```sql
SELECT pk_alumnos
FROM alumnos
WHERE pk_alumnos = 'alumnos';

```
## 16. Cree un índice compuesto por los campos "curso" y "materia", no único.

```sql
CREATE INDEX idx_curso_materia ON alumnos (curso, materia);

```
## 17. Verifique su existencia.

```sql
select * from alumnos;
```
## 18. Elimine la tabla "alumnos" y verifique que todos los índices han sido eliminados junto con ella.

```sql
DROP TABLE alumnos;

select * from alumnos;
```