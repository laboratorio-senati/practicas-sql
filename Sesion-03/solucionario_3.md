# Solucionario - Tipos de datos

## Ejercicios propuestos

# Ejercicio 01

## 1.Cree la tabla eligiendo el tipo de dato adecuado para cada campo.

codigo sql

```sql

CREATE TABLE peliculas (
  nombre VARCHAR(20),
  actor VARCHAR(20),
  duracion INT,
  cantidad_copias INT
);

```
salida sql:

```sh

Table PELICULAS creado.
```
## 2.Vea la estructura de la tabla.

codigo sql

```sql

DESCRIBE peliculas;
```
salida sql:

```sh

Nombre          ¿Nulo? Tipo         
--------------- ------ ------------ 
NOMBRE                 VARCHAR2(20) 
ACTOR                  VARCHAR2(20) 
DURACION               NUMBER(38)   
CANTIDAD_COPIAS        NUMBER(38) 
```
## 3.Ingrese los siguientes registros:

codigo sql

```sql

insert into peliculas (nombre, actor, duracion, cantidad_copias) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad_copias) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad_copias) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad_copias) values ('Elsa y Fred','China Zorrilla',110,2);

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
## 4.Muestre todos los registros (4 registros)

codigo sql

```sql

select * from peliculas;
```
salida sql:

```sh

NOMBRE               ACTOR                  DURACION CANTIDAD_COPIAS
-------------------- -------------------- ---------- ---------------
Mision imposible     Tom Cruise                  128               3
Mision imposible 2   Tom Cruise                  130               2
Mujer bonita         Julia Roberts               118               3
Elsa y Fred          China Zorrilla              110               2
```
## 5.Intente ingresar una película con valor de cantidad fuera del rango permitido:

codigo sql

```sql

 insert into peliculas (nombre, actor, duracion, cantidad_copias)
  values ('Mujer bonita','Richard Gere',1200,10);

```
salida sql:

```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
## 6.Muestre todos los registros para ver cómo se almacenó el último registro ingresado.

codigo sql

```sql

select * from peliculas;
```
salida sql:

```sh

NOMBRE               ACTOR                  DURACION CANTIDAD_COPIAS
-------------------- -------------------- ---------- ---------------
Mision imposible     Tom Cruise                  128               3
Mision imposible 2   Tom Cruise                  130               2
Mujer bonita         Julia Roberts               118               3
Elsa y Fred          China Zorrilla              110               2
Mujer bonita         Richard Gere               1200              10
```
# Ejercicio 02

## 1.Cree la tabla eligiendo el tipo de dato adecuado para cada campo:
codigo sql:

```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);

```
salida sql:

```sh
Table EMPLEADOS creado.
```
## 2.Verifique que la tabla existe consultando
codigo sql:

```sql
describe empleados;
```
salida sql:

```sh
Nombre       ¿Nulo? Tipo         
------------ ------ ------------ 
NOMBRE              VARCHAR2(20) 
DOCUMENTO           VARCHAR2(8)  
SEXO                VARCHAR2(1)  
DOMICILIO           VARCHAR2(30) 
SUELDOBASICO        NUMBER(6,2)  
```
## 3.Vea la estructura de la tabla (5 campos)
codigo sql:

```sql
describe empleados;
```
salida sql:

```sh
Nombre       ¿Nulo? Tipo         
------------ ------ ------------ 
NOMBRE              VARCHAR2(20) 
DOCUMENTO           VARCHAR2(8)  
SEXO                VARCHAR2(1)  
DOMICILIO           VARCHAR2(30) 
SUELDOBASICO        NUMBER(6,2)  
```
## 4.Ingrese algunos registros:
codigo sql:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);

```
salida sql:

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
## 5.Seleccione todos los registros (3 registros)
codigo sql:

```sql
select * from empleados;
```
salida sql:

```sh
NOMBRE               DOCUMENT S DOMICILIO                      SUELDOBASICO
-------------------- -------- - ------------------------------ ------------
Juan Perez           22333444 m Sarmiento 123                           500
Ana Acosta           24555666 f Colon 134                               650
Bartolome Barrios    27888999 m Urquiza 479  
```
## 6.Intente ingresar un registro con el valor "masculino" en el campo "sexo".
## Un mensaje indica que el campo está definido para almacenar 1 solo caracter como máximo y está intentando ingresar 9 caracteres.
codigo sql:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('lian lopez','24583566','m','Colon 124',750);
```
salida sql:

```sh
1 fila insertadas.
```
## 7.Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico" Mensaje de error.
codigo sql:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('marta agilar','24583599','f','Colon 127',10000000000);
```
salida sql:

```sh
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.
```