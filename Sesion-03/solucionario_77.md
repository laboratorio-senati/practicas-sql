# Solucionario -  Subconsulta simil autocombinacion

## Ejercicios propuestos 

# 1.Elimine la tabla:

```sql
drop table deportes;

```
# 2.Cree la tabla:

```sql
create table deportes(
    nombre varchar2(15),
    profesor varchar2(30),
    dia varchar2(10),
    cuota number(5,2)
);

```
# 3.Ingrese algunos registros. Incluya profesores que dicten más de un curso:

```sql
insert into deportes values('tenis','Ana Lopez','lunes',20);
insert into deportes values('natacion','Ana Lopez','martes',15);
insert into deportes values('futbol','Carlos Fuentes','miercoles',10);
insert into deportes values('basquet','Gaston Garcia','jueves',15);
insert into deportes values('padle','Juan Huerta','lunes',15);
insert into deportes values('handball','Juan Huerta','martes',10);

```
# 4.Muestre los nombres de los profesores que dictan más de un deporte empleando subconsulta (2 registros)

```sql
select distinct d1.profesor
from deportes d1
where d1.profesor in
(select d2.profesor
from deportes d2 
where d1.nombre <> d2.nombre);
```
# 5.Obtenga el mismo resultado empleando join

```sql
 select distinct d1.profesor from deportes d1 join deportes d2 on d1.profesor=d2.profesor where d1.nombre<>d2.nombre;
```
# 6.Buscamos todos los deportes que se dictan el mismo día que un determinado deporte (natacion) empleando subconsulta (1 registro)

```sql
 select nombre from deportes where nombre<>'natacion' and dia = (select dia from deportes  where nombre='natacion');
```
# 7.Obtenga la misma salida empleando "join"

```sql
select d1.nombre from deportes d1 join deportes d2 on d1.dia=d2.dia where d2.nombre='natacion' and d1.nombre<>d2.nombre;
```
