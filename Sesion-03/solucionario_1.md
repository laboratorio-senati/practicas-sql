# solucionario - Crear tablas (create table - describe - all_tables - drop table)

## Practica de laboratorio

# Ejercicio 01

## 2.intenta crear una tabla llamada agenda. 

Codigo sql:

```sql
  create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```
la salida:

```sh
Error que empieza en la línea: 1 del comando :
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
)
Informe de error -
ORA-00903: nombre de tabla no válido
00903. 00000 -  "invalid table name"
*Cause:    
*Action:
```
### En este ejercico nos muestra un error por que la tabla es invalida.

## 3.cree la tabla llamada agenda:
codigo sql:

```sql

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
La salida:

```sh

Table AGENDA creado.

```
## 6.Visualiza la estructura de la tabla agenda.
Codigo sql:

```sql

DESCRIBE agenda;
```
la salida:

```sh

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 
```

# Ejercicio_02

## 3.creae una tabla llamada libros:
Codigo sql:

```sql

create table libros (
  titulo varchar2(20),
  autor varchar2(30),
  editorial varchar2(15)
);

```
la salida:

```sh

Table LIBROS creado.

```
## 6.Visualice la estructura de la tabla "libros"

Codigo sql;

```sql

DESCRIBE libros;
```
la salida:

```sh

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 

```
## 7.Elimine la tabla libros:
Codigo sql:

```sql

drop table libros;

```
la salida:

```sh

Table LIBROS borrado.

```

