# solucionario - Operadores relacionales (is null)

## Practica de laboratorio

# Ejercicio 01

## 1.Elimine la tabla y créela con la siguiente estructura:
codio sql:

```sql
drop table medicamentos;

create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```
salida sql:

```sh
Table MEDICAMENTOS creado.

```
## 2.Visualice la estructura de la tabla "medicamentos"
codigo sql:

```sql
describe medicamentos;
```
salida sql:

```sh
Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3)  
```
## 3.Ingrese algunos registros con valores "null" para los campos que lo admitan:
codigo sql:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);
```
salida sql:

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
## 4.Vea todos los registros.
codigo sql:

```sql
select * from medicamentos;
```
salida sql:

```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         3 Buscapina            Roche                                  200
         1 Sertal gotas                                                100
         2 Sertal compuesto                                 8,9        150
         3 Buscapina            Roche                                  200

```
## 5.Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.
codigo sql

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Sertal compuesto',' ',null,150);

```
salida sql:

```sh
1 fila insertadas.
```

## 7.Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
codigo sql:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(5,null,null,null,150);

```
salida sql:

```sh
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("HR"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 8.Recupere los registros que contengan valor "null" en el campo "laboratorio" (3 registros)
codigo sql:

```sql
select * from medicamentos where laboratorio  is null;
```
salida sql:

```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         2 Sertal compuesto                                 8,9        150

```
## 9.Recupere los registros que contengan valor "null" en el campo "precio", luego los que tengan el valor 0 en el mismo campo. Note que el resultado es distinto (2 y 1 registros respectivamente)
codigo sql:

```sql
select * from medicamentos where precio is null;
```
salida sql:

```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         3 Buscapina            Roche                                  200
         4 Sertal compuesto                                            150
         4                                                             150

```
## 10.Recupere los registros cuyo laboratorio no contenga valor nulo (1 registro)
codigo sql:

```sql
SELECT * FROM medicamentos WHERE laboratorio IS NOT NULL;

```
salida sql:

```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         3 Buscapina            Roche                                  200
         4 Sertal compuesto                                            150
         4                                                             150

```
# Ejercicio 02

## 1.Elimine la tabla:
codigo sql:

```sql
drop table peliculas;
```
salida sql:

```sh
Table PELICULAS borrado.
```
## 2.Créela con la siguiente estructura:
codigo sql:

```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);

```
salida sql:

```sh
Table PELICULAS creado.
```
## 3.Visualice la estructura de la tabla. note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".
codigo sql:

```sql
DESCRIBE peliculas;
```
salida sql:

```sh
Nombre   ¿Nulo?   Tipo         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3)   
```
## 4.Ingrese los siguientes registros:
codigo sql:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);

```
salida sql:

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
## 5.Recupere todos los registros para ver cómo Oracle los almacenó.
codigo sql:

```sql
select * from peliculas;
```
salida sql:

```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal                                   180
         3 Harry Potter y la camara secreta         Daniel R.                      
         0 Mision imposible 2                                                   150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts            0

6 filas seleccionadas. 

```
## 6.Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)
codigo sql:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(null,'null','Caprio',220);

```
salida sql:

```sh
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("HR"."PELICULAS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 7.Muestre los registros con valor nulo en el campo "actor" (2 registros)
codigo sql:

```sql
SELECT * FROM peliculas WHERE actor IS NULL;

```
salida sql:

```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         2 Harry Potter y la piedra filosofal                                   180
         0 Mision imposible 2                                                   150

```
## 8.Actualice los registros que tengan valor de duración desconocido (nulo) por "120" (1 registro actualizado)
codigo sql:

```sql
update peliculas set duracion = 120 where duracion is null;
```
salida sql:

```sh
1 fila actualizadas.
```
## 9.Coloque 'Desconocido' en el campo "actor" en los registros que tengan valor nulo en dicho campo (2 registros)
codigo sql:

```sql
update peliculas set actor ='Desconocido' where actor is null;
```
salida sql:

```sh
2 filas actualizadas.
```
## 10.Muestre todos los registros
codigo sql:

```sql
select * from peliculas;
```
salida sql:

```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal       Desconocido                 180
         3 Harry Potter y la camara secreta         Daniel R.                   120
         0 Mision imposible 2                       Desconocido                 150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts            0
         6 null                                     Caprio                      220

7 filas seleccionadas. 


```