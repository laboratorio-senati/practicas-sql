# Solucionario - Restricción unique
# Ejercicios propuestos

## 1. Elimine la tabla:

```sql
drop table remis;
```

## 2. Cree la tabla con la siguiente estructura:

```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);
```

## 3. Ingrese algunos registros, 2 de ellos con patente repetida y alguno con patente nula.

```sql
insert into remis(numero,patente,marca,modelo) values ('1','4','13','set');
insert into remis(numero,patente,marca,modelo) values ('4','4','9','t');
insert into remis(numero,patente,marca,modelo) values ('5','4','5','k');
insert into remis(numero,patente,marca,modelo) values ('2',null,'10','get');
insert into remis(numero,patente,marca,modelo) values ('3','2','14','lal');
```
## 4. Agregue una restricción "primary key" para el campo "numero".

```sql
ALTER TABLE remis
ADD CONSTRAINT pk_remis PRIMARY KEY (numero);

```
## 5. Intente agregar una restricción "unique" para asegurarse que la patente del remis no tomará valores repetidos .No se puede porque hay valores duplicados, un mensaje indica que se encontraron claves duplicadas.

```sql
DELETE FROM remis
WHERE rowid NOT IN (
    SELECT MIN(rowid)
    FROM remis
    GROUP BY patente
);

```
## 6. Elimine el registro con patente duplicada y establezca la restricción. Note que hay 1 registro con valor nulo en "patente".

```sql
DELETE FROM remis
WHERE rowid NOT IN (
    SELECT MIN(rowid)
    FROM remis
    WHERE patente IS NOT NULL
    GROUP BY patente
);
```
## 7. Intente ingresar un registro con patente repetida (no lo permite)

```sql
insert into remis(numero,patente,marca,modelo) values ('6','4','7','et');

```
## 8. Ingrese un registro con valor nulo para el campo "patente".

```sql
insert into remis(numero,patente,marca,modelo) values ('7',null,'13','se');

```
