# Solucionario - Restricciones (foreign key)

# Ejercicios propuestos

Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.

## 1. Elimine las tablas "clientes" y "provincias" y créelas:

```sql
drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20)
);
```

En este ejemplo, el campo "codigoprovincia" de "clientes" es una clave foránea, se emplea para enlazar la tabla "clientes" con "provincias".

## 2. Intente agregar una restricción "foreign key" a la tabla "clientes" que haga referencia al campo "codigo" de "provincias"

```sql
ALTER TABLE clientes ADD CONSTRAINT fk_clientes_provincias
FOREIGN KEY (codigoprovincia) REFERENCES provincias (codigo);

```

## 3. Establezca una restricción "unique" al campo "codigo" de "provincias"

```sql
ALTER TABLE provincias ADD CONSTRAINT codigo
UNIQUE (codigo);

```
## 4. Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);
insert into clientes values(103,'Luisa Lopez','Juarez 555','La Plata',6);
```

## 5. Intente agregar la restricción "foreign key" del punto 2 a la tabla "clientes"

```sql
ALTER TABLE clientes ADD CONSTRAINT fk_clientes_provincias
FOREIGN KEY (codigoprovincia) REFERENCES provincias (codigo);

```

## 6. Elimine el registro de "clientes" que no cumple con la restricción y establezca la restricción nuevamente.

```sql
DELETE FROM clientes
WHERE codigoprovincia NOT IN (SELECT codigo FROM provincias);

```
## 7. Intente agregar un cliente con un código de provincia inexistente en "provincias"

```sql
INSERT INTO clientes (nombre, codigoprovincia)
VALUES ('Juan Pérez', '99');
```
## 8. Intente eliminar el registro con código 3, de "provincias".

```sql
DELETE FROM provincias WHERE codigo = 3;
 
```

## 9. Elimine el registro con código "4" de "provincias" Se permite porque en "clientes" ningún registro hace referencia a él.

```sql
DELETE FROM provincias WHERE codigo = 4;

```
## 10. Intente modificar el registro con código 1, de "provincias"

```sql
UPDATE provincias SET nombre = 'jhojan'
WHERE codigo = 1;

```

## 11. Vea las restricciones de "clientes" consultando "user_constraints"

```sql
SELECT nombre, codigoprovincia FROM clientes
WHERE clientes = 'clientes';

```
## 12. Vea las restricciones de "provincias"

```sql
SELECT * FROM provincias
WHERE codigo = 'provincias';

```
## 13. Intente eliminar la tabla "provincias" (mensaje de error)

```sql
DROP TABLE provincias;
```
