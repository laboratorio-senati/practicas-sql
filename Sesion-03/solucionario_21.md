# Solucionario - Funciones matemáticas

## Ejercicios de laboratorio

## 1.Eliminamos la tabla:
codigo sql:

```sql
drop table empleados;
```
## 2.Creamos la tabla:
codigo sql:

```sql
create table empleados(
    legajo number(5),
    documento char(8) not null,
    nombre varchar2(30) not null,
    domicilio varchar2(30),
    sueldo number(6,2),
    hijos number(2),
    primary key (legajo)
);

```
## 3.Ingresamos algunos registros:
codigo sql:

```sql
insert into empleados
values(1,'22333444','Ana Acosta','Avellaneda 213',870.79,2);

insert into empleados
values(20,'27888999','Betina Bustos','Bulnes 345',950.85,1);

insert into empleados
values(31,'30111222','Carlos Caseres','Caseros 985',1190,0);

insert into empleados
values(39,'33444555','Daniel Duarte','Dominicana 345',1250.56,3);

```
## 4.Vamos a mostrar los sueldos de los empleados redondeando el valor hacia abajo y luego hacia arriba (empleamos "floor" y "ceil"):
codigo sql:

```sql
select floor(sueldo) as "sueldo hacia abajo", ceil(sueldo) as "sueldo hacia arriba"
from empleados;

```
## 5.Mostramos los nombre de cada empleado, su respectivo sueldo, y el sueldo redondeando el valor a entero ("round") y truncado a entero ("trunc"):
codigo sql:

```sql
select nombre, sueldo, round(sueldo) as "sueldo redondeado", trunc(sueldo) as "sueldo truncado"
from empleados;

```
## 6.Mostramos el resultado de "2 elevado a la potencia 5" ("power"):
codigo sql:

```sql
select power(2,5) from dual;

```
## 7.Mostramos el resto de la división "1234 / 5" ("mod"):
codigo sql:

```sql
select mod(1234,5) from dual;

```
## 8.Calculamos la raíz cuadrada de 81:
codigo sql:

```sql
select sqrt(81) from dual;

```

