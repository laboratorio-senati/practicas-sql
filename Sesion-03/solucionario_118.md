# Solucionario - Usuarios (crear)

## Ejercicios propuestos

## 1.Primero eliminamos el usuario "director", porque si existe, aparecerá un mensaje de error:

```sql
drop user director cascade;

```
## 2.Cree un usuario "director", con contraseña "escuela" y 100M de espacio en "system"

```sql

```
## 3.Elimine el usuario "profesor":

```sql
drop user profesor cascade;

```
## 4.Cree un usuario "profesor", con contraseña "maestro" y espacio en "system"

```sql

```
## 5.Consulte el diccionario "dba_users" y analice la información que nos muestra

```sql

```
