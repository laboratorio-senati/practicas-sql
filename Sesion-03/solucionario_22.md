# Solucionario - Funciones de fechas y horas

## Ejercicios de laboratorio

## 1.Eliminamos la tabla:
codigo sql:

```sql
drop table libros;
```
## 2.Creamos la tabla:
codigo sql:

```sql
create table libros(
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);

```
## 3.Ingresamos algunos registros:
codigo sql:

```sql
insert into libros
values('El aleph','Borges','Emece','10/10/1980',25.33);

insert into libros
values('Java en 10 minutos','Mario Molina','Siglo XXI','05/05/2000',50.65);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Emece','08/09/2000',19.95);

insert into libros
values('Aprenda PHP','Mario Molina','Siglo XXI','02/04/2000',45);

```
## 4.Mostramos el título del libro y el año de edición:
codigo sql:

```sql
select titulo, extract (year from edicion) from libros;

```
## 5.Mostramos el título del libro y el mes de edición:
codigo sql:

```sql
select titulo, extract (month from edicion) from libros;

```
## 6.Mostramos el título del libro y los años que tienen de editados:
codigo sql:

```sql
select titulo, extract(year from sysdate)-extract(year from edicion) as "años de editado"
from libros;

```
## 7.Mostramos los títulos de los libros que se editaron en el año 2000:
codigo sql:

```sql
select titulo from libros
where extract(year from edicion)=2000;

```
## 8.Calcule 3 meses luego de la fecha actual empleando "add_months":
codigo sql:

```sql
select add_months(sysdate,3) from dual;

```
## 9.Muestre la fecha del primer martes desde la fecha actual:
codigo sql:

```sql
select next_day(sysdate,'martes') from dual;

```
## 10.Muestre la fecha que será 15 días después de "24/08/2018" empleando el operador "+":
codigo sql:

```sql
select to_date('24/08/2018')+15 from dual;

```
## 11.Muestre la fecha que 20 días antes del "12/08/2018" empleando el operador "-":
codigo sql:

```sql
select to_date('12/08/2018')-20 from dual;

```
