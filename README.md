# PRACTICAS-SQL

## Estudiante: Jhojan Ichuta Pacco

### solucionarios

[1. Crear tablas (create table - describe - all_tables - drop table)](Sesion-03/solucionario_1.md)

[2. Ingresar registros (insert into- select)](Sesion-03/solucionario_2.md)

[3. Tipos de datos](Sesion-03/solucionario_3.md)

[4. Recuperar algunos campos (select)](Sesion-03/solucionario_4.md)

[5. Recuperar algunos registros (where)](Sesion-03/solucionario_5.md)

[6. Operadores relacionales](Sesion-03/solucionario_6.md)

[7. Borrar registros (delete)](Sesion-03/solucionario_7.md)

[8. Actualizar registros (update)](Sesion-03/solucionario_8.md)

[9. Comentarios](Sesion-03/solucionario_9.md)

[10. Valores nulos (null)](Sesion-03/solucionario_10.md)

[11. Operadores relacionales (is null)](Sesion-03/solucionario_11.md)

[12. Clave primaria (primary key)](Sesion-03/solucionario_12.md)

[13. Vaciar la tabla (truncate table)](Sesion-03/solucionario_13.md)

[14.Tipos de datos alfanuméricos](Sesion-03/solucionario_14.md)

[15. Tipos de datos numéricos](Sesion-03/solucionario_15.md)

[16.Ingresar algunos campos](Sesion-03/solucionario_16.md)

[17.Valores por defecto (default)](Sesion-03/solucionario_17.md)

[18.Operadores aritméticos y de concatenación (columnas calculadas)](Sesion-03/solucionario_18.md)

[19. Alias (encabezados de columnas)](Sesion-03/solucionario_19.md)

[20. Funciones string](Sesion-03/solucionario_20.md)

[21.Funciones matemáticas](Sesion-03/solucionario_21.md)

[22. Funciones de fechas y horas](Sesion-03/solucionario_22.md)

[23. Ordenar registros (order by)](Sesion-03/solucionario_23.md)

[24. Operadores lógicos (and - or - not)](Sesion-03/solucionario_24.md)

[25. Otros operadores relacionales (between)](Sesion-03/solucionario_25.md)

[26. Otros operadores relacionales (in)](Sesion-03/solucionario_26.md)

[27. Búsqueda de patrones (like - not like)](Sesion-03/solucionario_27.md)

[28. Contar registros (count)](Sesion-03/solucionario_28.md)

[30. Agrupar registros (group by)](Sesion-03/solucionario_30.md)

[31. Seleccionar grupos (Having)](Sesion-03/solucionario_31.md)

[32. Registros duplicados (Distinct)](Sesion-03/solucionario_32.md)

[33. Clave primaria compuesta](Sesion-03/solucionario_33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](Sesion-03/solucionario_34.md)
[35. Alterar secuencia (alter sequence)](Sesion-03/solucionario_35.md)

[36. Integridad de datos](Sesion-03/solucionario_36.md)

[37. Restricción primary key](Sesion-03/solucionario_37.md)

[38. Restricción unique](Sesion-03/solucionario_38.md)

[39. Restriccioncheck](Sesion-03/solucionario_39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](Sesion-03/solucionario_40.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](Sesion-03/solucionario_41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](Sesion-03/solucionario_42.md)

[43. Indices](Sesion-03/solucionario_43.md)

[44. Indices (Crear . Información)](Sesion-03/solucionario_44.md)

[45. Indices (eliminar)](Sesion-03/solucionario_45.md)

[46. Varias tablas (join)](Sesion-03/solucionario_46.md)

[47. Combinación interna (join)](Sesion-03/solucionario_47.md)

[48. Combinación externa izquierda (left join)](Sesion-03/solucionario_48.md)

[49. Combinación externa derecha (right join)](Sesion-03/solucionario_49.md)

[50. Combinación externa completa (full join)](Sesion-03/solucionario_50.md)

[51. Combinaciones cruzadas (cross)](Sesion-03/solucionario_51.md)

[52. Autocombinación](Sesion-03/solucionario_52.md)

[53. Combinaciones y funciones de agrupamiento](Sesion-03/solucionario_53.md)

[54. Combinar más de 2 tablas](Sesion-03/solucionario_54.md)

[55. Otros tipos de combinaciones](Sesion-03/solucionario_55.md)

[56. Clave foránea](Sesion-03/solucionario_56.md)

[57. Restricciones (foreign key)](Sesion-03/solucionario_57.md)

[58. Restricciones foreign key en la misma tabla](Sesion-03/solucionario_58.md)

[59. Restricciones foreign key (eliminación)](Sesion-03/solucionario_59.md)

[60. Restricciones foreign key deshabilitar y validar](Sesion-03/solucionario_60.md)

[61. Restricciones foreign key (acciones)](Sesion-03/solucionario_61.md)

[62. Información de user_constraints](Sesion-03/solucionario_62.md)

[63. Restricciones al crear la tabla](Sesion-03/solucionario_63.md)

[64. Unión](Sesion-03/solucionario_64.md)

[65. Intersección](Sesion-03/solucionario_65.md)

[66. Minus](Sesion-03/solucionario_66.md)

[67. Agregar campos (alter table-add)](Sesion-03/solucionario_67.md)

[68. Modificar campos (alter table - modify)](Sesion-03/solucionario_68.md)

[69. Eliminar campos (alter table - drop)](Sesion-03/solucionario_69.md)

[70.  Agregar campos y restricciones (alter table)](Sesion-03/solucionario_70.md)

[71. Subconsultas](Sesion-03/solucionario_71.md)

[72. Subconsultas como expresion](Sesion-03/solucionario_72.md)

[73. Subconsultas con in](Sesion-03/solucionario_73.md)

[74. Subconsultas any- some - all](Sesion-03/solucionario_74.md)

[75. Subconsultas correlacionadas](Sesion-03/solucionario_75.md)

[76. Exists y No Exists](Sesion-03/solucionario_76.md)

[77. Subconsulta simil autocombinacion](Sesion-03/solucionario_77.md)

[78. Subconsulta conupdate y delete](Sesion-03/solucionario_78.md)

[79. Subconsulta e insert](Sesion-03/solucionario_79.md)

[80. Crear tabla a partir de otra (create table-select)](Sesion-03/solucionario_80.md)

[81. Vistas (create view)](Sesion-03/solucionario_81.md)

[82. Vistas (información)](Sesion-03/solucionario_82.md)

[83. Vistas eliminar (drop view)](Sesion-03/solucionario_83.md)

[84. Vistas (modificar datos a través de ella)](Sesion-03/solucionario_84.md)

[85. Vistas (with read only)](Sesion-03/solucionario_85.md)

[86. Vistas modificar (create or replace view)](Sesion-03/solucionario_86.md)

[87. Vistas (with check option)](Sesion-03/solucionario_87.md)

[88. Vistas (otras consideraciones: force)](Sesion-03/solucionario_88.md)

[89. Vistas materializadas (materialized view)](Sesion-03/solucionario_89.md)

[90. Procedimientos almacenados](Sesion-03/solucionario_90.md)

[91. Procedimientos Almacenados (crear- ejecutar)](Sesion-03/solucionario_91.md)

[92. Procedimientos Almacenados (eliminar)](Sesion-03/solucionario_92.md)

[93. Procedimientos almacenados (parámetros de entrada)](Sesion-03/solucionario_93.md)

[94. Procedimientos almacenados (variables)](Sesion-03/solucionario_94.md)

[95. Procedimientos Almacenados (informacion)](Sesion-03/solucionario_95.md)

[96. Funciones](Sesion-03/solucionario_96.md)

[97. Control de flujo (if)](Sesion-03/solucionario_97.md)

[98. Control de flujo (case)](Sesion-03/solucionario_98.md)

[99. Control de flujo (loop)](Sesion-03/solucionario_99.md)

[100. Control de flujo (for)](Sesion-03/solucionario_100.md)

[101. Control de flujo (bucle while)](Sesion-03/solucionario_101.md)

[102. Disparador (gatillo)](Sesion-03/solucionario_102.md)

[103. Disparador (información)](Sesion-03/solucionario_103.md)

[104. Disparador de insercion a nivel de sentencia](Sesion-03/solucionario_104.md)

[105. Disparador de inserción a nivel de fila (insertar gatillo para cada fila)](Sesion-03/solucionario_105.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](Sesion-03/solucionario_106.md)

[107. Disparador de actualización a nivel de sentencia (update trigger)](Sesion-03/solucionario_107.md)

[108. Disparador de actualización a nivel de fila (update trigger)](Sesion-03/solucionario_108.md)

[109. Disparador de actualización - lista de campos (update trigger)](Sesion-03/solucionario_109.md)

[110. Disparador de multiples eventos](Sesion-03/solucionario_110.md)

[111. Disparador (old y new)](Sesion-03/solucionario_111.md)

[112. Disparador condiciones (when)](Sesion-03/solucionario_112.md)

[113. Disparador de actualizacion - campos (updating)](Sesion-03/solucionario_113.md)

[114. Disparadores (habilitar y deshabilitar)](Sesion-03/solucionario_114.md)

[115. Disparador (eliminar)](Sesion-03/solucionario_115.md)

[116. Errores definidos por el usuario en trigger](Sesion-03/solucionario_116.md)

[117. Seguridad y acceso a Oracle](Sesion-03/solucionario_117.md)

[118. Usuarios (crear)](Sesion-03/solucionario_118.md)

[119. Permiso de conexión](Sesion-03/solucionario_119.md)

[120. Privilegios del sistema (conceder)](Sesion-03/solucionario_120.md)

[121. Privilegios del sistema (with admin option)](Sesion-03/solucionario_121.md)

[122. Modelado de base de datos](Sesion-03/solucionario_122.md)


### Practicas SQL

[practicas](taller/practicas_sql.md)